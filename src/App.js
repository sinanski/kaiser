import React from 'react';
import Router from "./game/Router";
import { Provider } from 'react-redux';
import ReactTooltip from "react-tooltip";
import store from "./store";
import './assets/css/style.css';
import './assets/css/rpg-awesome.css';
import './assets/css/game-font.css';

function App() {
  return (
    <Provider store={store}>
      <ReactTooltip multiline delayShow={400} />
      <Router />
    </Provider>
  );
}

export default App;
