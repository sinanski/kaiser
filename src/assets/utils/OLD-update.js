export function amount(change, current, max = Infinity, min = 0) {
  let updated = updateWithinMaxRange(change, current, max);
  updated = updateWithinMinRange(updated, min)
  return updated;
}

function updateWithinMaxRange(change, current, max) {
  const result = current + change;
  return result < max
    ? change
    : max - current
}

function updateWithinMinRange(value, min) {
  return value > min ? value : min;
}
