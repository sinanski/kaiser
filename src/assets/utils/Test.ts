import Player from "../../player/Player";
const player = new Player();

type freeLandFunction = () => number;

interface DefinedPlayer {
    freeLand: freeLandFunction;
}

export default class Comp {
    one: string;
    two: object;
    constructor(one:string, two: object) {
        this.one = one;
        this.two = two;
    }
}