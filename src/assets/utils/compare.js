export default {
  getName: (needs, player) => compare(needs, player, pushMissingName),
  getObject: (needs, player) => compare(needs, player, pushMissingObject),
  getMissing: (needs, player) => compare(needs, player, pushLackingObject),
}

function pushMissingObject(key, value) {
  return { [key]: value }
}

function pushMissingName(key) {
  return key
}

function pushLackingObject(key, needs, player) {
  const has = player[key];
  const value = needs - has > 0 ? needs - has : 0;
  return { [key]: value }
}

function compare(needs, player, onMissing) {
  let playerLacks = [];
  for ( let key in needs ) {
    const needed = needs[key];
    const playerHas = checkPlayerProperty(player, key);
    if ( needed > playerHas ) {
      const toPush = onMissing(key, needed, player);
      playerLacks.push(toPush)
    }
  }
  return playerLacks;
}

function checkPlayerProperty(player, key) {
  if ( key !== 'land' ) {
    return player[key] || 0;
  }
  return player.freeLand(player)
}
