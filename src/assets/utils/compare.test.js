import compare from './compare';
import Player from "../../player/Player";

const needs = {money_amount: 100, land_amount: 10};
const player = new Player({money_amount: 50, land_amount: 8});

describe('utils/compare', () => {
  it('getName() returns an array of strings with the names of missing attributes', () => {
    const result = compare.getName(needs, player);
    expect(result).toEqual(['money_amount', 'land_amount']);
    expect(result).toHaveLength(2);
  });

  it('getObject() returns an array of objects {missing: amount_needed}', () => {
    const result = compare.getObject(needs, player);
    expect(result).toEqual([{money_amount: 100}, {land_amount: 10}]);
    expect(result).toHaveLength(2);

  });

  it('getMissing() returns an array of objects {missing: amount_missing}', () => {
    const result = compare.getMissing(needs, player);
    console.log(result)
    expect(result).toEqual([{money_amount: 50}, {land_amount: 2}]);
    expect(result).toHaveLength(2);

  });
});
