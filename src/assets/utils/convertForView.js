import goldImage from '../img/gold.png';
import cornImage from '../img/food.png';
import landImage from '../img/land.png';
import peopleImage from '../img/people.png';
import imigrantsImage from '../img/rotten.png';
import deathImage from '../img/rotten.png';
import fieldImage from '../img/corn.png';
import marketImage from '../img/market.png';
import windmillImage from '../img/windmill.png';
import rottenImage from '../img/rotten.png';
import cityImage from '../img/city.png';
import kDot from './kDot';

const images = {
  corn: cornImage,
  corn_amount: cornImage,
  money: goldImage,
  money_amount: goldImage,
  land: landImage,
  land_amount: landImage,
  people: peopleImage,
  people_amount: peopleImage,
  field: fieldImage,
  market: marketImage,
  windmill: windmillImage,
  city: cityImage,
  rotten: rottenImage,
  imigrants: imigrantsImage,
  death: deathImage,
};

const translation = {
  corn: 'Korn',
  corn_amount: 'Korn',
  money: 'Geld',
  money_amount: 'Geld',
  land: 'Land',
  land_amount: 'Land',
  land_max: 'Land Maximum',
  people: 'Einwohner',
  people_amount: 'Einwohner',
  field: 'Felder',
  market: 'Märkte',
  windmill: 'Windmühlen',
  rotten: 'verrottet',
};

const icon_name = {
  corn: 'acorn',
  corn_amount: 'acorn',
  money: 'gold-bar',
  money_amount: 'gold-bar',
  land: 'wooden-sign',
  land_amount: 'wooden-sign',
  land_max: 'wooden-sign',
  people: 'double-team',
  people_amount: 'double-team',
  up: 'arrow-up'
};

export function fromValues(name, amount) {
  return resources({[name]: amount})[0]
}

export function resources(resourceObject) {
  console.log(resourceObject)
  return Object.keys(resourceObject)
  .map(name => {
    const amount = resourceObject[name];
    return new Resource(name, amount);
  })
}

function Resource(name, amount) {
  this.name = translation[name];
  this.type = name;
  this.image = images[name];
  this.amount = amount;
  this.amount_dot = kDot(amount);
  this.icon_name = icon_name[name];
  this.does_raise_max = name.endsWith('_max');
  this.raise_icon = icon_name.up;
}
