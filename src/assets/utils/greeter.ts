interface Person {
    firstName: string;
    lastName: string;
}

export default class Student {
    fullName: string;
    constructor(
        public firstName: string,
        public middleInitial: string,
        public lastName: string
    ) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}


// export default function greeter(person: Person) {
//     console.log(person);
//     return "Hello, " + person.firstName + " " + person.lastName + '  ---  '// + person.fullName;
//     //return "Hello, " + person.firstName + " " + person.lastName;
// }

//let user = new Student("Jane", "M.", "User");
// let user = new Student("Jane", "M.", "User");
//
// document.body.textContent = greeter(user);