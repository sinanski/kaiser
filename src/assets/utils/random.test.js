import * as random from './random'

describe('random', () => {
  test('raise', () => {
    const raise = random.raise(1, 100);
    expect(random.raise(100, 100)).toBe(100);
    expect(random.raise(10, 20)).toBeGreaterThanOrEqual(10);
    expect(random.raise(10, 20)).toBeLessThanOrEqual(20);
  })
})