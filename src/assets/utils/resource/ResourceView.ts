import { images } from "./images";
import { translation } from "./translation";
import { icon_name } from "./icons";

export class ResourceView {
    // resource: object | string;
    // amount: number;
    constructor(
        public resource: object | string,
        public amount: string,
    ) {
        // this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}