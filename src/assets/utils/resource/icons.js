export const icon_name = {
  corn: 'acorn',
  corn_amount: 'acorn',
  money: 'gold-bar',
  money_amount: 'gold-bar',
  land: 'wooden-sign',
  land_amount: 'wooden-sign',
  land_max: 'wooden-sign',
  people: 'double-team',
  people_amount: 'double-team',
  up: 'arrow-up'
};