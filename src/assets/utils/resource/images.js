import goldImage from '../../img/gold.png';
import cornImage from '../../img/food.png';
import landImage from '../../img/land.png';
import peopleImage from '../../img/people.png';
import imigrantsImage from '../../img/rotten.png';
import deathImage from '../../img/rotten.png';
import fieldImage from '../../img/corn.png';
import marketImage from '../../img/market.png';
import windmillImage from '../../img/windmill.png';
import rottenImage from '../../img/rotten.png';
import cityImage from '../../img/city.png';

export const images = {
  corn: cornImage,
  corn_amount: cornImage,
  money: goldImage,
  money_amount: goldImage,
  land: landImage,
  land_amount: landImage,
  people: peopleImage,
  people_amount: peopleImage,
  field: fieldImage,
  market: marketImage,
  windmill: windmillImage,
  city: cityImage,
  rotten: rottenImage,
  imigrants: imigrantsImage,
  death: deathImage,
};