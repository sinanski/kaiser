export function amount(change:number, current:number, max?:number, min?:number):number {
    let updated = updateWithinMaxRange(change, current, max);
    updated = updateWithinMinRange(updated, min);
    return updated;
}

function updateWithinMaxRange(change:number, current:number, max = Infinity):number {
    const result = current + change;
    return result < max
        ? change
        : max - current
}

function updateWithinMinRange(value:number, min = 0):number {
    return value > min ? value : min;
}
