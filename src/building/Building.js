import compare from "../assets/utils/compare";
import defaultPlayer from "../player/defaultPlayer";
import PlayerMethods from "../player/methods/PlayerMethods";
import Reward from "../player/methods/Reward";
import * as convertForView from '../assets/utils/convertForView';

const playerMethods = new PlayerMethods();

export default class Building {
  constructor({
    type,
    name,
    plural,
    description,
    partsNeeded = 0,
    enableDecrement = true,
    cost = {},
    dependencies = {},
    produces = {}, // per Turn
    reward = {} // on Finish building
              }) {
    this.type = type;
    this.name = name;
    this.plural = plural || name;
    this.description = description;
    this.partsNeeded = partsNeeded;
    this.enableDecrement = enableDecrement;
    this.cost = cost;
    this.shown_cost = convertForView.resources(this.cost);
    this.dependencies = dependencies;
    this.produces = convertResourceKeys(produces);
    this.reward = convertResourceKeys(reward);
  }

  checkResources(player) {
    // Hier muss noch das freie Land geprüft werden
    // const playerLacks = compare(this.cost, playerResources, pushMissingName);
    // const resourceLack = compare.getName(this.cost, player);
    // const hasFreeLand = checkForFreeLand(this.cost, player);
    // console.log(resourceLack)
    // console.log(hasFreeLand)
    // console.log('----')
    // const playerLacks = hasFreeLand
    //   ? resourceLack
    //   : resourceLack.concat('land_amount')
    // return {
    //   canAfford: !playerLacks.length,
    //   playerLacks
    // }
    return checkResources(player, this.cost)
  }

  checkDependencies(player) {
    const playerLacks = compare.getObject(this.dependencies, player);
    return {
      isMet: !playerLacks.length,
      playerLacks
    }
  }

  build(player, amount = 1) {
    const cost = getCostWithoutLand(this.cost);
    const playerAfterPay = playerMethods.updatePlayer(player, cost, -1);
    const updatedPlayer = mergePlayerAfterBuild(playerAfterPay, this.type, amount);
    const rewarded = new Reward(updatedPlayer, this.reward);
    return rewarded.next;
  }

  unbuild(player) {
  //  should return this.pay(player, 0.8) and unbuilding update
  //  So the building is removed and 80% of the costs are returned
  }

  // getReward(player) {
  //   return playerMethods.updatePlayer(player, this.reward)
  // }

  perTurn(amount, multiplier = 1) {
    let change = this.produces;
    for ( let i = 1; i < amount; i++ ) {
      change = getChangePerTurn(change, this.produces)
    }
    return change;
  }

}

function checkForFreeLand(cost, player) {
  const freeLand = playerMethods.freeLand(player);
  return freeLand >= cost.land_amount || 0;
}

function mergePlayerAfterBuild(player, type, amount) {
  const newAmount = player.property[type] + amount;
  return {
    ...player,
    property: {
      ...player.property,
      [type]: newAmount
    }
  }
}

function checkResources(player, cost) {
  const resourceLack = compare.getName(cost, player);
  const hasFreeLand = checkForFreeLand(cost, player);
  const playerLacks = listOfLackingStats(resourceLack, hasFreeLand);
  return {
    canAfford: !playerLacks.length,
    playerLacks
  }
}

function listOfLackingStats(resourceLack, hasFreeLand) {
  const isAlreadyLackingLand = resourceLack.indexOf('land_amount') !== -1;
  if ( hasFreeLand || isAlreadyLackingLand ) return resourceLack;
  return resourceLack.concat('land_amount')
}

function getCostWithoutLand(cost) {
  let cleanCost = {}
  for ( let i in cost ) {
    if ( i !== 'land_amount' ) {
      cleanCost = {
        ...cleanCost,
        [i]: cost[i]
      }
    }
  }
  return cleanCost;
}

function CustomError(message) {
  this.message = message;
};

function convertResourceKeys(cost) {
  let newCost = {};
  for ( let i in cost ) {
    const errorInKey = defaultPlayer[i] === undefined && defaultPlayer[i + '_amount'] === undefined;
    if ( errorInKey ) throw new CustomError(`${i} does not exist as a stat in player`);
    const key = defaultPlayer[i]
      ? i
      : i + '_amount';
    newCost = {
      ...newCost,
      [key]: cost[i]
    };
  }
  return newCost
}

function getChangePerTurn(change, buildingGain) {
  let newVal = change
  for ( let key in buildingGain ) {
    newVal = {
      ...newVal,
      [key]: newVal[key] + buildingGain[key]
    }
  }
  return newVal
}
