import Building from "./Building";
import Player from "../player/Player";
import buildings from '../constants/buildings';
import MergedPlayer from "../player/methods/MergedPlayer";

const city = new Building(buildings.city);
const { cost = {}, reward = {}, dependencies = {} } = buildings.city;
const { money = 1000, land = 100 } = cost;


describe('Building CheckResources for city', () => {
  test('If player has all resources canAfford is true', () => {
    const resources = { money_amount: 50000, land_amount: 50 };
    const check = city.checkResources(new MergedPlayer(resources));
    expect(check).toHaveProperty('canAfford', true)
  });
  test('If player lacks money canAfford is false', () => {
    const resources = { money_amount: 1, land_amount: 100 };
    const check = city.checkResources(new MergedPlayer(resources));
    expect(check).toHaveProperty('canAfford', false)
  });
  test('If player lacks money checkResources.playerLacks = [money]', () => {
    const resources = { money_amount: 1, land_amount: 100 };
    const check = city.checkResources(new MergedPlayer(resources));
    expect(check.playerLacks.length).toBe(1);
    expect(check.playerLacks).toContain('money_amount')
  });
  test('If player lacks money and land checkResources.playerLacks = [money, land]', () => {
    // Does not work with
    // const resources = { money_amount: 1, land_amount: 1 };
    // Array contains land_amount twice
    const resources = { money_amount: 1, land_amount: 1 };
    const check = city.checkResources(new MergedPlayer(resources));
    expect(check.playerLacks.length).toBe(2);
    expect(check.playerLacks).toEqual(expect.arrayContaining(['money_amount', 'land_amount']))
  });
});

describe('CheckDependencies', () => {
  test('If player does not meet dependencies an array of the dependencies is returned that is not met', () => {
    const player = { people_amount: 500 };
    const buildDependencies = city.checkDependencies(player);
    expect(buildDependencies).toHaveProperty('isMet', false)
    expect(buildDependencies.playerLacks).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
        people_amount: dependencies.people_amount
      })
    ]))
  });
  test('city is active if dependencies are met', () => {
    const city = new Building(buildings.city);
    const needed_people_amount = city.dependencies.people_amount;
    const player1 = new MergedPlayer({people_amount: 1});
    const player2 = new MergedPlayer({people_amount: needed_people_amount});
    const dependencies1 = city.checkDependencies(player1);
    const dependencies2 = city.checkDependencies(player2);
    const expectedLack = [{people_amount: needed_people_amount}]
    const expected1 = {
      isMet: false,
      playerLacks: expectedLack
    };
    expect(dependencies1).toMatchObject(expected1)
    expect(dependencies2).toMatchObject({isMet: true})
  })
});

// test('cost is converted from money to money_amount', () => {
//   const testBuilding = new Building({cost: {money: 10}})
//   expect(testBuilding.cost).toHaveProperty('money_amount', 10)
// });

test('Get Reward returns the players updated with the reward', () => {
  // TODO Needs to get MergedPlayer with reward method triggered.
  // const player = new MergedPlayer({
  //   money_amount: 23000,
  //   land_amount: 40,
  //   people_max: 20
  // });
  // const testBuildingConfig = {
  //   reward: {
  //     'money': 2000,
  //     'land': 10,
  //     'people_max': 10
  //   },
  // }
  // const building = new Building(testBuildingConfig)
  // const playerAfterReward = building.getReward(player);
  // const expected = {
  //   money_amount: 25000,
  //   land_amount: 50,
  //   people_max: 30,
  // };
  // expect(playerAfterReward).toMatchObject(expected);
});

test('Building.perTurn expects the amount of buildings and an optional multiplier and returns the raised stats ', () => {
  const field = new Building(buildings.field);
  const amount = 5;
  const expectedAmount = buildings.field.produces.corn_amount * amount;
  const raise = field.perTurn(amount);
  expect(raise).toHaveProperty('corn_amount', expectedAmount);
});

test('Building.build returns new MergedPlayer object', () => {
  const field = new Building(buildings.field);
  const player = new MergedPlayer();
  const updatedPlayer = field.build(player);
  const expected = player.money_amount - buildings.field.cost.money_amount;
  expect(updatedPlayer.money_amount).toBe(expected)
  expect(updatedPlayer.property.field).toBe(player.property.field + 1)
});

test('building a city provides expected rewards', () => {
  const city = new Building(buildings.city);
  const player = new MergedPlayer();
  const updatedPlayer = city.build(player);
  const expected_people_max = player.attributes.people_max + city.reward.people_max;
  const expected_money_amount = player.money_amount - city.cost.money_amount;
  const expected = {
    ...player,
    money_amount: expected_money_amount,
    attributes: {
      ...player.attributes,
      people_max: expected_people_max
    },
    property: {
      ...player.property,
      city: 1
    }
  }
  expect(updatedPlayer).toMatchObject(expected)
});
