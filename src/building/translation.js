export default {
  people: 'Einwohner',
  people_amount: 'Einwohner',
  people_max: 'Einwohner können nun in eurem Reich leben.',
  money: 'Geld',
  money_amount: 'Geld',
  land: 'Land',
  land_amount: 'Land',
  land_max: 'maximale Landfläche',
}