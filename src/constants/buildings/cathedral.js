const cathedral = {
  type: 'cathedral',
  name: 'Kathedrale',
  plural: 'Kathedralen',
  description: 'Ihr benötigt eine Kathedrale um Kaiser zu werden',
  partsNeeded: 20,
  enableDecrement: false,
  cost: {
    'money_amount': 15000,
    'land_amount': 1
  },
  dependencies: {
    'people_amount': 2500,
  },
};

export default cathedral;
