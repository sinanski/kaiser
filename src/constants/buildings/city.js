const city = {
  type: 'city',
  name: 'Stadt',
  plural: 'Städte',
  description: 'Städte erhöhen euer Maximum an Einwohnern',
  partsNeeded: 5,
  enableDecrement: false,
  cost: {
    'money_amount': 20000,
    'land_amount': 20
  },
  dependencies: {
    'people_amount': 1000,
  },
  reward: {
    'people_max': 10000,
    'statue_max': 1,
  },
  produces: {
    'money_amount': 2500,
  },
};

export default city;
