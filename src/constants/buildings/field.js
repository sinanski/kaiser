const field = {
  type: 'field',
  name: 'Feld',
  plural: 'Felder',
  description: 'Felder erhöhen die Menge an Korn die Ihr jede Runde erhaltet',
  cost: {
    'money_amount': 200,
    'land_amount': 1
  },
  produces: {
    'corn_amount': 30,
  },
};

export default field;