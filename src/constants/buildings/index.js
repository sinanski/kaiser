import city from "./city";
import field from "./field";
import market from "./market";
import statue from "./statue";
import windmill from "./windmill";

export default {
  city,
  field,
  market,
  statue,
  windmill
};
