const market = {
  type: 'market',
  name: 'Markt',
  plural: 'Märkte',
  description: 'Märkte bringen euch in jeder Runde Geld',
  cost: {
    'money_amount': 1000,
    'land_amount': 1
  },
  produces: {
    'money_amount': 250,
  },
};

export default market;
