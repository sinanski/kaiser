const palace = {
  type: 'palace',
  name: 'Pallast',
  plural: 'Palläste',
  description: 'Ihr benötigt einen Pallast um Kaiser zu werden',
  partsNeeded: 25,
  enableDecrement: false,
  cost: {
    'money_amount': 20000,
    'land_amount': 10
  },
  dependencies: {
    'people_amount': 5000,
  },
};

export default palace;
