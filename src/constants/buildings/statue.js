const statue = {
  type: 'statue',
  name: 'Statue',
  plural: 'Statuen',
  description: 'Statuen bringen euch einmalig Presitge',
  cost: {
    'money_amount': 50000,
    'land_amount': 1
  },
  reward: {
    'prestige_amount': 3,
  },
};

export default statue;