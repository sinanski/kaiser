const windmill = {
  type: 'windmill',
  name: 'Windmühle',
  plural: 'Windmühlen',
  description: 'Windmühlen lassen euch mehr Korn speichern und produzieren Korn pro Runde',
  cost: {
    'money_amount': 2000,
    'land_amount': 1
  },
  produces: {
    'corn_amount': 40,
  },
  reward: {
    'corn_max': 150
  }
};

export default windmill;
