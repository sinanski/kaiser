// export const blue900 = '#0c1025';
export const blue900 = '#161825';
export const blue800 = '#202541';
export const blue600 = '#24294a';
export const blue400 = '#2d325a';
export const blue200 = '#5c6290';
export const light_blue = '#4e7cff';
export const light_blue_hover = '#6d90f5';
export const font = '#6b72a4';
export const light_grey = '#d4d4d4';
// export const icon = '#7479a0';



export const green = '#41cd7d';
export const yellow = '#feba11';
export const orange = '#dc7754';
export const red = '#f65164';