import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as playerActions from '../player/actions';
import * as gameActions from './actions';
import Turn from "../turn/Turn";
import Phase from "../turn/Phase";
import GameView from "./GameView";

const phase = new Phase();

class Game extends PureComponent {

  constructor(props) {
    super(props);
    const { dispatch } = props;
    const allActions = { ...playerActions, ...gameActions };
    this.boundActionCreators = bindActionCreators(allActions, dispatch);
    this.state = {
      turn_index: 1,
      phase: phase.start,
      turn: new Turn(props.player),
    }
  }

  onProgress = () => {
    const isLast = phase.isLast(this.state.phase);
    if ( isLast ) {
      this.nextTurn()
    } else {
      this.nextPhase()
    }
  };

  nextPhase() {
    const nextPhase = phase.next(this.state.phase);
    this.setState({ phase: nextPhase })
  }

  nextTurn() {
    const { turn_index } = this.state;
    this.setState({
      turn_index: turn_index + 1,
      phase: phase.reStart(),
      turn: new Turn(this.props.player, turn_index + 1)
    })
  }

  render() {
    const {
      phase,
      turn,
    } = this.state;
    return (
      <GameView
        turn={turn}
        phase={phase}
        game={this.props.game}
        boundActions={this.boundActionCreators}
        onProgress={ this.onProgress }
      />
    )
  }
}

const mapStateToProps = state => ({
  active: state.players.active,
  player: state.players.listOfPlayers[state.players.active],
  game: state.game,
});

export default connect(mapStateToProps)(Game)
