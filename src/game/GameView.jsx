import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Sidebar from "../hud/side-bar";
import SideNav from '../hud/side-nav';
import MainRouter from "./MainRouter";
import * as colors from '../constants/colors';
import ProgressButton from "../hud/ProgressButton";

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  min-width: 100vw;
  background-color: ${colors.blue600};
  color: ${colors.font};
`;

const StaticInterface = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const GameView = (props) => (
    <Container>
      <SideNav />
      <Sidebar turn={props.turn} />
      <StaticInterface>
        <MainRouter
          { ...props }
        />
        <ProgressButton
          onClick={props.onProgress}
          disabled={props.game.isProgressDisabled}
        />
      </StaticInterface>
    </Container>
);

GameView.propTypes = {
  onProgress: PropTypes.func.isRequired,
  game: PropTypes.object.isRequired,
};

export default GameView;
