import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Build from "./screens/build/Build";
import Start from "./screens/start/Start";
import CornAndLand from "./screens/corn_and_land";
import Birth from "./screens/birth";
import Level from "./screens/level";
import { ViewBox } from "./screens/Styles";

const Container = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

function renderPhase({
  turn,
  phase,
  boundActions,
  onProgress
}) {
  switch (phase) {
    case 'start':
      return <Start turn={turn} />;
    case 'corn_and_land':
      return <CornAndLand actions={boundActions} turn={turn} />;
    case 'birth':
      return <Birth resetProvideFood={boundActions.resetProvideFood} />;
    case 'build':
      return <Build />;
    case 'level':
      return <Level skip={onProgress} />;
    default:
  }
}

const MainRouter = (props) => (
  <Container>
    <ViewBox>
      { renderPhase(props) }
    </ViewBox>
  </Container>
);

MainRouter.propTypes = {
  turn: PropTypes.object.isRequired,
  phase: PropTypes.string.isRequired,
  onProgress: PropTypes.func.isRequired,
  boundActions: PropTypes.shape({
    enableProgress: PropTypes.func.isRequired,
    disableProgress: PropTypes.func.isRequired,
    storeProvidedFoodRatio: PropTypes.func.isRequired,
    resetProvideFood: PropTypes.func.isRequired,
  })
};

export default MainRouter;
