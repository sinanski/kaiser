import React, {PureComponent} from 'react';
import { connect } from 'react-redux';
import SetupPlayers from "./opening/SetupPlayers";
import Game from "./Game";

class Router extends PureComponent {

  state = {

  }

  render() {
    if ( !this.props.players.listOfPlayers.length ) {
      return (
        <SetupPlayers/>
      )
    }
    return (
      <Game />
    )
  }
}

Router.propTypes = {};

const mapStateToProps = state => ({
  players: state.players,
});

const mapDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(Router);
