export function disableProgress() {
  return {
    type: 'DISABLE_PROGRESS',
  };
}

export function enableProgress() {
  return {
    type: 'ENABLE_PROGRESS',
  };
}

export function routeSidebar(route) {
  return {
    type: 'ROUTE_SIDEBAR',
    route
  };
}