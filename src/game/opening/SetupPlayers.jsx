import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Player from "../../player/Player";
// import Player from "../../player/Player";
import { connect } from 'react-redux';
import { setPlayers } from "../../player/actions";

const Container = styled.div`
  //display: flex;
`;

const Part = styled.div`
  display: flex;
`;

const NameInput = ({ name, onChange }) => (
  <input
    value={ name }
    onChange={ onChange }
  />
);

class SetupPlayers extends PureComponent {

  state = {
    amount_of_players: 0,
    isAmountSet: false,
    playerNames: [],
  };

  componentDidMount(){
    this.playerAmount.focus();
  }

  onSetPlayerAmount = e => {
    this.setState({
      amount_of_players: e.target.value,
    })
  };

  onSetName(value, index) {
    this.setState({
      playerNames: [
        ...this.state.playerNames.slice(0, index),
        value,
        ...this.state.playerNames.slice(index + 1),
      ]
    })
  }

  renderPlayerNameInput = () => {
    const { amount_of_players, playerNames } = this.state;
    let listOfPlayers = [];
    for ( let i = 0; i < amount_of_players; i++ ) {
      listOfPlayers.push(
        <NameInput
          name={playerNames[i] || ''}
          onChange={e => this.onSetName(e.target.value, i)}
        />
      )
    }
    return listOfPlayers;
  };

  onFinish = () => {
    const players = this.state.playerNames.map(name => (
      new Player({name})
    ));
    this.props.dispatch(
      setPlayers( players )
    )
  };



  render() {
    const { amount_of_players, isAmountSet, playerNames } = this.state;
    return (
      <Container>
        <Part>
        <p>
          Wie viele Spieler spielen Kaiser?
        </p>
        <input
          ref={(input) => { this.playerAmount = input; }}
          value={amount_of_players}
          onChange={this.onSetPlayerAmount}
        />
        <button
          disabled={!amount_of_players}
          onClick={ () => this.setState({isAmountSet: true}) }
        >
          Go!
        </button>
        </Part>
        { isAmountSet &&
          <div>
            <Part>
              {this.renderPlayerNameInput().map((input, i) => (
                <div key={i}>
                  {input}
                </div>
              ))}
            </Part>
            <button
              disabled={ playerNames.length < amount_of_players }
              onClick={ this.onFinish }
            >
              Start Game
            </button>
          </div>
        }
      </Container>
    )
  }
}

SetupPlayers.propTypes = {};

export default connect()(SetupPlayers)
