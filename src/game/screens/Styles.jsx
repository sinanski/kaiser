import styled from 'styled-components';

export const ViewBox = styled.div`
  display: flex;
  max-width: 600px;
`;
