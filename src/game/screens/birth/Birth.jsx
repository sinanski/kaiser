import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import connectToPlayer from "../../../player/connectToPlayer";
import Box from "../../../hud/styles/Box";
import FullStat from "../../../hud/elements/FullStat";

const Container = styled.div`
width: 100%;
min-width: 240px;
`;

class Birth extends PureComponent {

  constructor(props) {
    super(props);
    this.newPeople = props.provideFood();
  }

  componentWillUnmount() {
    const people_amount = this.newPeople.raise;
    this.props.changePlayer({people_amount})
    this.props.resetProvideFood()
  }

  render() {
    const {
      birth,
      death,
      imigrants,
      raise,
      isLimitExceeded,
      overflow
    } = this.newPeople;
    return (
      <Container>
        <Box
          bottomLine
          bottom={
            <FullStat
              value={raise}
              icon='player'
              tooltip='Gesamt'
            />
          }
        >
          <p className="headline">
            Bevölkerung
          </p>
          <FullStat
            value={birth}
            icon='player'
            tooltip='Einwohner die geboren wurden'
            description='Geboren'
          />

          <FullStat
            value={death}
            icon='player-pain'
            tooltip='Einwohner die starben'
            description='Gestorben'
          />

          <FullStat
            value={imigrants}
            icon='aware'
            tooltip='Menschen die in Euer Reich einwanderten'
            description='Eingewandert'
          />

          { isLimitExceeded &&
            <FullStat
              value={overflow}
              icon='player-dodge'
              tooltip='Menschen die nicht mehr aufgenommen werden konnten'
              description='Überzählig'
            />
          }
        </Box>
      </Container>
    )
  }
}

Birth.propTypes = {
  resetProvideFood: PropTypes.func.isRequired,
};

export default connectToPlayer(Birth);
