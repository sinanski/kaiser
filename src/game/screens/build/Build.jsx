import React, {PureComponent} from 'react';
import styled from 'styled-components';
import BuildButton from "../../../hud/elements/card/BuildCard";
import fieldImage from '../../../assets/img/corn.png';
import marketImage from '../../../assets/img/market.png';
import windmillImage from '../../../assets/img/windmill.png';
import cityImage from '../../../assets/img/city.png';
import field from '../../../constants/buildings/field';
import market from '../../../constants/buildings/market';
import windmill from "../../../constants/buildings/windmill";
import city from "../../../constants/buildings/city";
// import statue from "../../constants/buildings/statue";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

class Build extends PureComponent {
  render() {
    return (
      <Container>
        <Row>
          <BuildButton img={fieldImage} building={field} />
          <BuildButton img={marketImage} building={market} />
          <BuildButton img={windmillImage} building={windmill} />
        </Row>
        <Row>
          <BuildButton img={cityImage} building={city} />
        </Row>
      </Container>
    )
  }
}

export default Build
