import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import connectToPlayer from "../../../player/connectToPlayer";
import FullStat from "../../../hud/elements/FullStat";
import ButtonGroup from "../../../hud/elements/ButtonGroup";
import CornAndLandView from "./CornAndLandView";

const translation = {
  min: 'Geizig',
  med: 'Benötigtes',
  max: 'Großzügig'
};

const ProvideButton = ({
  needed,
  onClick,
  isActive,
  text
}) => (
  <ButtonGroup.Button
    disabled={needed.isMoreThanAvailable}
    showWarning={needed.isMoreThanMax}
    onClick={onClick}
    isActive={ isActive }
  >
    <div>{text}</div>
    <FullStat value={needed.amount} icon='acorn' noMargin color='white' />
  </ButtonGroup.Button>
);

class CornAndLand extends PureComponent {

  componentDidMount() {
    const { disableProgress } = this.props.actions;
    disableProgress()
  }

  getNeeded(provided_food) {
    const { foodDemand } = this.props;
    return foodDemand(provided_food);
  }

  onChoose = chosen_amount => {
    const { storeProvidedFoodRatio, enableProgress } = this.props.actions;
    storeProvidedFoodRatio(chosen_amount);
    enableProgress();
  };

  button = amount_button => {
    const { provided_food } = this.props;
    const needed = this.getNeeded(amount_button);
    const isActive = provided_food === amount_button;
    const text = translation[amount_button];
    return (
      <ProvideButton
        needed={needed}
        onClick={() => this.onChoose(amount_button)}
        isActive={isActive}
        text={text}
      />
    )
  };

  componentWillUnmount() {
    const { provided_food, changePlayer } = this.props;
    const corn_amount = this.getNeeded(provided_food).amount * -1;
    changePlayer({ corn_amount })
  }

  render() {
    return (
      <CornAndLandView
        turn={this.props.turn}
        provideButton={this.button}
      />
    )
  }
}

CornAndLand.propTypes = {
  turn: PropTypes.object.isRequired,
};

export default connectToPlayer(CornAndLand);
