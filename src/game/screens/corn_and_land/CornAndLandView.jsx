import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Box from "../../../hud/styles/Box";
import ButtonGroup from "../../../hud/elements/ButtonGroup";
import CornCard from "../../../hud/elements/card/CornCard";
import LandCard from "../../../hud/elements/card/LandCard";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
  margin-bottom: 40px;
`;

const CornAndLandView = ({
  provideButton,
  turn
                         }) => (
  <Container>
    <Row>
      <CornCard turn={turn} />
      <LandCard turn={turn} />
    </Row>
    <p className="headline">
      Gebt Eurem Volk Nahrung
    </p>
    <Box>
      <p className="headline--small">
        Wählt wieviel Ihr geben wollt
      </p>
      <ButtonGroup>
        {provideButton('min')}
        {provideButton('med')}
        {provideButton('max')}
      </ButtonGroup>
    </Box>
  </Container>
);

CornAndLandView.propTypes = {
  turn: PropTypes.object.isRequired,
  provideButton: PropTypes.func.isRequired,
};

export default CornAndLandView;
