import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import connectToPlayer from "../../../player/connectToPlayer";
import Box from "../../../hud/styles/Box";
import PropertyBox from "../../../hud/elements/PropertyBox";
import Button from "../../../hud/elements/Button";
import CardView from "../../../hud/elements/card/view/CardView";
import {fromValues} from "../../../assets/utils/convertForView";

const buy_corn_amount = 100;

const translation = {
  min: 'Geizig',
  med: 'Das Benötigte',
  max: 'Großzügig'
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Flex = styled.div`
  display: flex;
`;

const ContainerX = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;


class CornAndLand extends PureComponent {


  componentDidMount() {
    const { disableProgress } = this.props.actions;
    disableProgress()
  }

  getNeeded(provided_food) {
    const { foodDemand } = this.props;
    return foodDemand(provided_food);
  }

  onChoose = chosen_amount => {
    const { storeProvidedFoodRatio, enableProgress } = this.props.actions;
    storeProvidedFoodRatio(chosen_amount);
    enableProgress();
  };

  button(amount_button) {

    const { provided_food } = this.props;
    const needed = this.getNeeded(amount_button);
    const isActive = provided_food === amount_button;
    const text = translation[amount_button];
    return (
      <Button
        disabled={needed.isMoreThanAvailable}
        showWarning={needed.isMoreThanMax}
        onClick={() => this.onChoose(amount_button)}
        isActive={ isActive }
      >
        {`${text} (${needed.amount})`}
      </Button>
    )
  }

  componentWillUnmount() {
    const { provided_food, changePlayer } = this.props;
    const corn_amount = this.getNeeded(provided_food).amount * -1;
    changePlayer({ corn_amount })
  }

  render() {
    const {
      canBuyCorn,
      canSellCorn,
      canBuyLand,
      canSellLand,
      turn,
      // price,
      player
    } = this.props;
    const { price } = turn;
    const shown_cost = fromValues('money',price.corn * buy_corn_amount)
    return (
      <Container>
        <ContainerX>
          <Row>

        <CardView
          onIncrement={() => this.props.buyCorn(price.corn, buy_corn_amount)}
          onDecrement={() => this.props.sellCorn(price.corn, buy_corn_amount)}
          enableDecrement={canSellCorn(buy_corn_amount)}
          tooltip={turn.corn.description}
          shown_cost={[shown_cost]}
          isActive
          icon='acorn'
          canAfford={canBuyCorn(price.corn)}
        />
            <CardView
              onIncrement={() => this.props.buyCorn(price.corn, buy_corn_amount)}
              onDecrement={() => this.props.sellCorn(price.corn, buy_corn_amount)}
              enableDecrement={canSellCorn(buy_corn_amount)}
              tooltip={turn.corn.description}
              shown_cost={[shown_cost]}
              isActive
              icon='acorn'
              canAfford={canBuyCorn(price.corn)}
            />
          </Row>
        </ContainerX>
        <Box>
          <Box.Center gapBottom>

            <Button
              name='verkaufen'
              disabled={!canSellCorn(buy_corn_amount)}
              onClick={() => this.props.sellCorn(price.corn, buy_corn_amount)}
            />
            <PropertyBox
              name='corn'
              tooltip={turn.corn.description}
              price={ price.corn * buy_corn_amount }
            />
            <Button
              name='kaufen'
              disabled={!canBuyCorn(price.corn)}
              onClick={() => this.props.buyCorn(price.corn, buy_corn_amount)}
            />
          </Box.Center>
          <Box.Center>
            <Button
              name='verkaufen'
              disabled={!canSellLand()}
              onClick={() => this.props.sellLand(price.land)}
            />
            <PropertyBox
              name='land'
              tooltip='Ihr benötigt Land zum bauen von Gebäuden und Feldern'
              price={ price.land }
            />
            <Button
              name='kaufen'
              disabled={!canBuyLand(price.land)}
              onClick={() => this.props.buyLand(price.land)}
            />
          </Box.Center>
        </Box>
        <Box>
          <Box.Center>
            <Flex>
              {this.button('min')}
              {this.button('med')}
              {this.button('max')}
            </Flex>

          </Box.Center>
        </Box>
      </Container>
    )
  }
}

CornAndLand.propTypes = {};

export default connectToPlayer(CornAndLand);
