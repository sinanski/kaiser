import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import connectToPlayer from "../../../player/connectToPlayer";
import LevelView from "./LevelView";

class Level extends PureComponent {

  componentDidMount() {
    if ( !this.rank().shouldRaise ) {
      this.props.skip()
    }
  }

  rank = () => {
    return this.props.player.rankUp
  };

  componentWillUnmount() {
    const { shouldRaise, player_next } = this.rank();
    if ( shouldRaise ) {
      this.props.updatePlayer(player_next)
    }
  }

  render() {
    const rank = this.rank();
    return (
      <LevelView
        name={this.props.player.name}
        title={rank.next.title}
        reward={rank.reward}
        following={rank.following}
      />
    )
  }
}

Level.propTypes = {
  skip: PropTypes.func.isRequired
}

export default connectToPlayer(Level);
