import React from 'react';
import PropTypes from 'prop-types';
import styled, {keyframes} from 'styled-components';
import Box from "../../../hud/styles/Box";
import * as colors from '../../../constants/colors';
import Resources from "./Resources";

const appear = keyframes`
  0% {
    transform: translateY(200px);
    opacity: 0;

  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
`;

const Delay = styled.div`
  position: relative;
  opacity: 0;
  animation: ${appear} .8s ease-out forwards 1s;
`;

const Container = styled.div`
  width: 100%;
`;

const Content = styled.div`
  font-size: 14px;
`;

const Congratulation = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 9px;
`;

const Icon = styled.i`
  //font-weight: bold;
  color: ${colors.yellow};
  font-size: 12px;
  padding-right: 8px;
`;

const Bold = styled.span`
  //font-weight: bold;
  color: white;
`;



const LevelView = ({title, name, reward, following}) => (
  <Container>
    <Box
      grow
      bottomLine
      bottom={
        <Resources reward={reward} />
      }
    >
      <Content>
        <Congratulation>
          <Icon className="game-star" />
          Ihr habt einen neuen Rang erreicht
        </Congratulation>
          <Bold>
            {title} {name}
          </Bold>
      </Content>
    </Box>
    <Delay>
      <Box
        grow
        bottomLine
        bottom={
          <Resources reward={following.reward} />
        }
      >
        <Content>
          Für den Rang {following.title} benötigt ihr
          <Resources reward={following.dependencies} />
        </Content>
      </Box>
    </Delay>

  </Container>
);

LevelView.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  reward: PropTypes.array.isRequired,
  following: PropTypes.object.isRequired,
};

export default LevelView
