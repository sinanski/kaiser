import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {fromValues} from "../../../assets/utils/convertForView";
import StatWithIcon from "../../../hud/elements/StatWithIcon";

const Container = styled.div`
  display: flex;
  padding: 0 20px;
`;

const Reward = styled.div`
  position:relative;
  display: flex;
  padding-left: 15px;
`;

const shownReward = reward => {
  return reward.map( single => {
    const name = Object.keys(single)[0];
    const amount = single[name];
    return fromValues(name, amount)
  })
};

const Resources = ({reward}) => (
  <Container>
    { shownReward(reward).map(({
       amount_dot,
       icon_name,
       does_raise_max,
       raise_icon
     }, i) => (
       <Reward key={i}>

         <StatWithIcon
           icon={icon_name}
           value={amount_dot}
           max={does_raise_max}
           secondaryColor='green'
         />
       </Reward>

    ))}
  </Container>
);

Resources.propTypes = {
  reward: PropTypes.array.isRequired
};

export default Resources;
