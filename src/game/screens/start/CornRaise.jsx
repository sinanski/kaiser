import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import StatWithIcon from "../../../hud/elements/StatWithIcon";
import Resource from "../../../hud/styles/Resource";
import buildings from "../../../constants/buildings";
import Box from "../../../hud/styles/Box";

const Container = styled.div`
  display: flex;
  //flex: 1 1 auto;
  flex: 0 1 225px;
`;

const CornRaise = ({corn, total_corn}) => (
  <Container>
    <Box
      bottom={<StatWithIcon value={total_corn} icon='acorn' noMargin/>}
      grow
      bottomLine
      alignEnd
    >
      <Resource
        name='field'
        value={corn.from_fields}
        tooltip={buildings.field.description}
      />
      <Resource
        name='windmill'
        value={corn.from_windmills}
        tooltip={buildings.windmill.description}
      />
      <Resource
        name='rotten'
        value={corn.rotten}
        tooltip='Die Menge an Korn, die verrottet ist'
      />
    </Box>
  </Container>
);

CornRaise.propTypes = {
  corn: PropTypes.object.isRequired,
  total_corn: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired
};

export default CornRaise
