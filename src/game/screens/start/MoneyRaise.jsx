import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import StatWithIcon from "../../../hud/elements/StatWithIcon";
import Resource from "../../../hud/styles/Resource";
import Box from "../../../hud/styles/Box";

const Container = styled.div`
  display: flex;
  flex: 0 1 225px;
`;

const MoneyRaise = ({sources, total_money}) => (
  <Container>
    <Box
      bottom={<StatWithIcon value={total_money} icon='gold-bar' noMargin/>}
      grow
      bottomLine
      alignEnd
    >
      <Resource
        name='market'
        value={sources.market}
        tooltip='Einnahmen durch Märkte'
      />
      <Resource
        name='money'
        value={sources.trade}
        tooltip='Einnahmen durch Handel'
      />
      <Resource
        name='money'
        value={sources.tax}
        tooltip='Einnahmen durch Steuern'
      />
    </Box>
  </Container>
);

MoneyRaise.propTypes = {
  total_money: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired,
  sources: PropTypes.shape({
    market: PropTypes.number.isRequired,
    trade: PropTypes.number.isRequired,
    tax: PropTypes.number.isRequired,
  }).isRequired
};

export default MoneyRaise
