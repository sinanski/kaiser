import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Box from "../../../hud/styles/Box";
import connectToPlayer from "../../../player/connectToPlayer";
import { fromValues } from '../../../assets/utils/convertForView';
import Icon from "../../../hud/Icon";
import CornRaise from "./CornRaise";
import MoneyRaise from "./MoneyRaise";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  
`;

const Weather = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  & i {
    padding-right: 20px;
  }
  & p {
    font-size: 30px;
    margin-bottom: 0;
  }
`;

class Start extends PureComponent {

  constructor(props) {
    super(props);
    const {player, turn} = props;
    this.income = turn.income(player);
    this.corn = turn.corn.raise;
  }

  componentWillUnmount() {
    const corn_amount = this.corn;
    const money_amount = this.income.total;
    this.props.changePlayer({corn_amount, money_amount});
  }

  render() {
    const { weather, corn } = this.props.turn;
    const { total, sources } = this.income;
    const total_money = fromValues('money', total).amount_dot;
    const total_corn = fromValues('corn', corn.raise).amount_dot;
    return (
      <Container>
        <Weather>
          <Icon name={weather.icon_name} size='5x' prefix='game' />
          <p>
            {weather.text}
          </p>
        </Weather>
        <Box.Row>
            <CornRaise
              total_corn={total_corn}
              corn={corn}
            />
            <MoneyRaise
              sources={sources}
              total_money={total_money}
            />
        </Box.Row>
      </Container>
    )
  }
}

Start.propTypes = {
  turn: PropTypes.object.isRequired,
};

export default connectToPlayer(Start);
