import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as colors from '../constants/colors';


const shadowColor = colors.blue400;
const shadow = `
-2px 0 ${shadowColor},
2px 0 ${shadowColor},
0 -2px ${shadowColor},
2px 1px ${shadowColor},
-1px 2px ${shadowColor},
2px -1px ${shadowColor}
`

const StyledIcon = styled.i`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-left: ${props => !props.noPadding ? '8px' : 0};
  color: ${ props => props.color
    ? colors[props.color] || props.color
    : colors.font
  };
  text-shadow: ${props => props.outline ? shadow : 'none'};
`;

const Icon = ({
  name,
  size = 'fw',
  prefix = 'ra',
  noPadding,
  color,
  outline,
}) => (
  <StyledIcon
    className={`${prefix} ra-${size} ${prefix}-${name}`}
    noPadding={noPadding}
    color={color}
    outline={outline}
  />
);

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.string,
  prefix: PropTypes.string,
  color: PropTypes.string,
  noPadding: PropTypes.bool,
  outline: PropTypes.bool,
};

export default Icon;
