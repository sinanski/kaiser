import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from "./elements/Button";

const Container = styled.div`
  position: fixed;
  display: flex;
  bottom: 13px;
  right: 13px;
  z-index: 500;
`;

const Icon = styled.i`
  font-size: 20px;
`;

const ProgressButton = ({
  onClick,
  disabled
                        }) => (
  <Container>
    <Button
      onClick={onClick}
      disabled={disabled}
      isActive
    >
      <Icon className='game-arrow-right'/>
    </Button>
  </Container>
);

ProgressButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default ProgressButton
