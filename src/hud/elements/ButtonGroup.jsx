import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as colors from '../../constants/colors';

const background = ({ isActive, disabled }) => (
  isActive
    ? !disabled
      ? colors.light_blue
      : 'none'
    : 'none'
);

const Container = styled.div`
  display: flex;
`;

const StyledButton = styled.button`
  display: flex;
  padding: 8px 19px;
  border: 1px solid ${props => !props.disabled ? colors.light_blue : colors.light_grey};
  background: ${ props => background(props) };
  color: white;
  cursor: ${props => !props.disabled ? 'pointer' : 'initial'};;
  opacity: ${props => !props.disabled ? 1 : .5};
  transition: background-color .5s ease;
  &:hover {
    background-color:${props => !props.disabled ? colors.light_blue : 'transparent'};
  }
  &:focus {
    outline: none;
  }
  &:active {
    outline: none;
    background-color: ${props => !props.disabled ? colors.light_blue_hover : 'transparent' };
  }
  &:first-of-type {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    border-right: none;
  }
  &:last-of-type {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    border-left: none;
  }
`;

const SingleButton = ({
  disabled,
  onClick,
  children,
  isActive,
  name
                      }) => (
  <StyledButton
    disabled={disabled}
    onClick={onClick}
    isActive={isActive}
  >
    {name || children || 'Button'}
  </StyledButton>
);

const ButtonGroup = ({
  children,
                }) => (
  <Container>
    {children}
  </Container>
);

ButtonGroup.Button = SingleButton;

SingleButton.propTypes = {
  name: PropTypes.string,
  disabled: PropTypes.bool,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export default ButtonGroup
