import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  overflow: hidden;
  border-radius: 16px;
  //border: 1px solid gray;
  box-shadow: 0 1px 9px rgba(0, 0, 0, 0.12);
`;

const Image = styled.img`
  width: 150px;
  height: 150px;
`;

const Button = styled.button`
  display: flex;
  border: none;
  background-color: lightblue;
  min-width: 52px;
  justify-content: center;
  &:active {
  background-color: red;
  }
`;

const BuyButton = ({
  img,
                   // TODO Selling is not yet possible
                   }) => (
  <Container>
    <Button>-</Button>
    <Image src={img} alt="corn"/>
    <Button>+</Button>
  </Container>
);

BuyButton.propTypes = {
  img: PropTypes.string,
};

export default BuyButton
