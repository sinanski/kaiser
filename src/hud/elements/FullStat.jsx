import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import StatWithIcon from "./StatWithIcon";

const Container = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${ props => props.noMargin ? 0 : '8px' };
`;

const Description = styled.div`
  font-size: 12px;
`;

const FullStat = (props) => {
  const {description, noMargin, ...rest} = props
  return (
    <Container noMargin={noMargin}>
      <Description>
        {description}
      </Description>
      <StatWithIcon {...rest} />
    </Container>
  )
};

FullStat.propTypes = {
  tooltip: PropTypes.string,
  secondaryColor: PropTypes.string,
  description: PropTypes.string,
  noMargin: PropTypes.bool,
  max: PropTypes.bool,
  pie: PropTypes.bool,
  star: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  icon: PropTypes.string.isRequired,
};

export default FullStat
