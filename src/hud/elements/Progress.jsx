import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import kDot from "../../assets/utils/kDot";
import * as colors from '../../constants/colors';

const Container = styled.div`
  display: flex;
  align-items: flex-end;
  position:relative;
  width: 100%;
  max-width: 200px;
  height: 15px;
  background: ${colors.blue600};
`;

const Content = styled.div`
  position:relative;
  width: 100%;
  height: 15px;
  overflow: hidden;
  // background: ${colors.blue600};
`;

const Bar = styled.div`
  position:absolute;
  width: ${props => props.length}%;
  max-width: 200px;
  height: 100%;
  background: ${colors.blue200};
  //opacity: .85;
`;

const Current = styled.div`
  position:absolute;
  max-width: 200px;
  height: 100%;
  font-size: 17px;
  opacity: .85;
  line-height: 1rem;
  z-index: 1;
`;

const Indicator = styled.div`
  background-color: ${props => props.color};
  height: 1px;
  flex: ${props => props.width ? '0 0 ' + props.width + '%' : '1 1 auto'};
`;

const IndicatorContainer = styled.div`
  position:relative;
  display: flex;
  width: 100%;
  height: 100%;
`;

// const green = '#91bd3a';
// const orange = '#ffa259';
// // const dark_orange = '#fe6845';
// const red = '#fa4252';

const Progress = ({current, max, indicator}) => (
  <Container>
    <Current>
      {kDot(current)}
    </Current>
    <Content>

      <Bar length={current/max * 100} />
      {indicator &&
        <IndicatorContainer>
          <Indicator color={colors.red} width={indicator.min}/>
          <Indicator color={colors.orange} width={indicator.med}/>
          <Indicator color={colors.green} />
        </IndicatorContainer>
      }
    </Content>

  </Container>
);

Progress.propTypes = {
  current: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  indicator: PropTypes.objectOf(PropTypes.number),
};

export default Progress
