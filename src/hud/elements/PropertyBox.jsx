import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { fromValues } from '../../assets/utils/convertForView';
import ReactTooltip from "react-tooltip";
import Icon from "../Icon";

const Container = styled.div`
  display: flex;
`;

const Values = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
`;

const Name = styled.div`
  font-size: 13px;
  padding-bottom: 3px;
`;

const Amount = styled.div`
  font-weight: bold;
  color: white;
`;

const PropertyBox = ({
  name,
  price,
  tooltip,
                     }) => {
  const resource = fromValues(name, price);
  return (
    <Container data-tip={tooltip}>
      <ReactTooltip />

      <Values>
        <Name>
          {resource.name}
        </Name>
        <Amount>
          {resource.amount_dot}
        </Amount>
      </Values>
      <Icon name={resource.icon_name} size='2x' />
    </Container>
  )
};

PropertyBox.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  tooltip: PropTypes.string,
};

export default PropertyBox
