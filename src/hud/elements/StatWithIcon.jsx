import React from 'react';
import PropTypes from 'prop-types';
import kDot from "../../assets/utils/kDot";
import { StatContainer, Value, Up } from "../side-bar/screens/overview/Styles";
import Icon from "../Icon";


const SecondaryIcon = ({name, color}) => {
  return (
    <Up>
      <Icon
        name={name}
        prefix='game'
        noPadding
        color={color}
        outline={!color}
      />
    </Up>
  )
}

const StatWithIcon = ({
  value = '',
  icon,
  tooltip,
  max,
  star,
  pie,
  color,
  secondaryColor,
}) => (
  <StatContainer data-tip={tooltip}>
    <Value>
      {kDot(value)}
    </Value>
    <Icon name={icon} color={color} />
    { pie &&
      <SecondaryIcon name='pie-graph' color={secondaryColor} />
    }
    { star &&
      <SecondaryIcon name='star' color={secondaryColor} />
    }
    { max &&
      <SecondaryIcon name='arrow-up' color={secondaryColor} />
    }
  </StatContainer>
);

StatWithIcon.propTypes = {
  tooltip: PropTypes.string,
  color: PropTypes.string,
  secondaryColor: PropTypes.string,
  max: PropTypes.bool,
  pie: PropTypes.bool,
  star: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  icon: PropTypes.string.isRequired,
};

export default StatWithIcon
