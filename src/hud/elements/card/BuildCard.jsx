import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Building from "../../../building/Building";
import CardView from "./view/CardView";
import connectToPlayer from '../../../player/connectToPlayer';


class BuildCard extends PureComponent {

  constructor(props) {
    super(props);
    this.building = new Building(props.building);
  }

  onIncrement = () => {
    const newPlayer = this.building.build(this.props.player)
    this.props.updatePlayer(newPlayer)
  };

  onDecrement = () => {
    console.warn('decrement not yet handled')
  };

  render() {
    const {
      img,
      player
    } = this.props;
    const {
      enableDecrement,
      description,
      type,
      shown_cost,
      name
    } = this.building;
    const resources = this.building.checkResources(player);
    const { canAfford, playerLacks } = resources;
    const lacking_tooltip = 'Euch fehlt es an ' + playerLacks.join(', ')
    const isActive = this.building.checkDependencies(player).isMet;
    return (
      <CardView
        name={name}
        isActive={ isActive }
        canAfford={ canAfford }
        img={ img }
        shown_cost={shown_cost || []}
        enableDecrement={ enableDecrement }
        tooltip={ canAfford ? description : lacking_tooltip }
        onIncrement={ this.onIncrement }
        onDecrement={ this.onDecrement }
        amount={ player.property[type] }
      />
    )
  }
}


BuildCard.propTypes = {
  img: PropTypes.string,
  building: PropTypes.object.isRequired,
  player: PropTypes.object.isRequired,
};

export default connectToPlayer(BuildCard)
