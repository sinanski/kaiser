import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CardView from "./view/CardView";
import connectToPlayer from '../../../player/connectToPlayer';
import {fromValues} from "../../../assets/utils/convertForView";

const buy_corn_amount = 100;

class CornCard extends PureComponent {

  onIncrement = () => {
    const { corn } = this.props.turn.price;
    this.props.buyCorn(corn, buy_corn_amount)
  };

  onDecrement = () => {
    const { corn } = this.props.turn.price;
    this.props.sellCorn(corn, buy_corn_amount)
  };

  shownCost = () => {
    const { corn } = this.props.turn.price;
    const shown_cost_corn = fromValues('money',corn * buy_corn_amount);
    return [ shown_cost_corn ];
  };

  enableIncrement = () => {
    const {
      canBuyCorn,
      turn,
    } = this.props;
    return canBuyCorn(turn.price.corn)
  };

  enableDecrement = () => {
    const { canSellCorn } = this.props;
    return canSellCorn(buy_corn_amount)
  };

  render() {
    const {
      turn,
      player,
    } = this.props;
    const amount = `${player.corn_amount} / ${player.corn_max}`;
    const tooltip = turn.corn.description;
    return (
      <CardView
        name='Korn'
        amount={amount}
        onIncrement={this.onIncrement}
        onDecrement={this.onDecrement}
        canAfford={this.enableIncrement()}
        enableDecrement={this.enableDecrement()}
        tooltip={tooltip}
        shown_cost={this.shownCost()}
        isActive
        icon='acorn'
      />
    )
  }
}

CornCard.propTypes = {
  turn: PropTypes.object.isRequired
};

export default connectToPlayer(CornCard)
