import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CardView from "./view/CardView";
import connectToPlayer from '../../../player/connectToPlayer';
import {fromValues} from "../../../assets/utils/convertForView";

class LandCard extends PureComponent {

  onIncrement = () => {
    const { land } = this.props.turn.price;
    this.props.buyLand(land)
  };

  onDecrement = () => {
    const { land } = this.props.turn.price;
    this.props.sellLand(land)
  };

  shownCost = () => {
    const { land } = this.props.turn.price;
    const shown_cost_land = fromValues('money', land);
    return [ shown_cost_land ];
  };

  enableIncrement = () => {
    const {
      canBuyLand,
      turn,
    } = this.props;
    return canBuyLand(turn.price.land)
  };

  enableDecrement = () => {
    const { canSellLand } = this.props;
    return canSellLand()
  };

  render() {
    const { player } = this.props;
    const amount = `${player.land_amount} / ${player.land_max}`;
    const tooltip = 'Ihr benötigt Land zum bauen von Gebäuden und Feldern';
    return (
      <CardView
        name='Land'
        amount={amount}
        onIncrement={this.onIncrement}
        onDecrement={this.onDecrement}
        canAfford={this.enableIncrement()}
        enableDecrement={this.enableDecrement()}
        tooltip={tooltip}
        shown_cost={this.shownCost()}
        isActive
        icon='wooden-sign'
      />
    )
  }
}

LandCard.propTypes = {
  turn: PropTypes.object.isRequired
};

export default connectToPlayer(LandCard);
