import styled from 'styled-components';
import * as colors from "../../../../constants/colors";

export const Container = styled.div`
  margin-right: 18px;
  opacity: ${ props => props.isActive ? '1' : .5 };
  pointer-events: ${ props => props.isActive ? 'initial' : 'none' };
  
`;

export const Content = styled.div`
  display: flex;
  width: 160px;
  flex-direction: column;
  align-items: center;
  padding-bottom: 4px;
`;

export const Preview = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Header = styled.div`
  display: flex;
  padding: 10px 20px 4px 20px;
  font-size: 12px;
  justify-content: space-between;
`;

export const Amount = styled.div`
  font-size: 14px;
`;

export const Image = styled.img`
  pointer-events: initial;
  width: 60px;
  height: 60px;
  padding: 10px 0;
  object-fit: contain;
  align-self: center;
  filter: ${ props => props.isActive ? 'grayscale(0)' : 'grayscale(.7)' };
`;

export const IconContainer = styled.div`
  pointer-events: initial;
  width: 60px;
  height: 60px;
  padding: 10px 0;
  object-fit: contain;
  align-self: center;
  // filter: ${ props => props.isActive ? 'grayscale(0)' : 'grayscale(.7)' };
`;

export const ButtonGroup = styled.div`
  display: flex;
  flex: 1 1 auto;
  height: 40px;
`;

export const Button = styled.button`
  pointer-events: ${props => props.disabled ? 'none' : 'initial'};;
  flex: 1 1 auto;
  cursor: pointer;
  border: none;
  background: ${props => props.isActive && props.disabled
    ? colors.blue800
    : 'transparent'
  };
  color: white;
  transition: background-color .5s ease;
  &:hover {
    background-color: ${colors.blue200};
  }
  &:focus {
    outline: none;
    background-color: ${colors.blue200};
  }
  &:active {
    outline: none;
  }
`;
