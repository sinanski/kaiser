import React from 'react';
import PropTypes from 'prop-types';
import Box from "../../../styles/Box";
import Icon from "../../../Icon";
import Cost from "./Cost";
import {
  Container,
  Content,
  Preview,
  Header,
  Amount,
  Image,
  IconContainer,
  ButtonGroup,
  Button
} from './CardStyles';

const CardView = ({
  isActive,
  img,
  icon,
  shown_cost,
  canAfford,
  enableDecrement,
  onIncrement,
  onDecrement,
  tooltip,
  amount,
  name
                         }) => (
  <Container isActive={isActive} >
    <Box
        noPadding
        bottomLine
        fullBottom
        bottom={
          <ButtonGroup>
            <Button
              isActive={isActive}
              disabled={!enableDecrement}
              onClick={onDecrement}
            >
              -
            </Button>
            <Button
              isActive={isActive}
              disabled={!canAfford}
              onClick={onIncrement}
            >
              +
            </Button>
          </ButtonGroup>
        }
      >
      <Content>
        <Preview>
          <Header>
            <div>
              {name}
            </div>
            {amount &&
              <Amount>
                {amount}
              </Amount>
            }
          </Header>
          { img &&
          <Image
            src={img}
            data-tip={tooltip}
            isActive={isActive}
          />
          }
          { icon &&
            <IconContainer>
              <Icon name={icon} size='3x' />
            </IconContainer>
          }
        </Preview>
        <Cost costList={shown_cost} />
      </Content>
    </Box>
  </Container>

);

CardView.propTypes = {
  isActive: PropTypes.bool.isRequired,
  canAfford: PropTypes.bool.isRequired,
  enableDecrement: PropTypes.bool.isRequired,
  img: PropTypes.string,
  icon: PropTypes.string,
  shown_cost: PropTypes.array.isRequired,
  tooltip: PropTypes.string.isRequired,
  amount: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
};

export default CardView
