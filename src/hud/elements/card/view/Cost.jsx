import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from "../../../Icon";

const Container = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-evenly;
  padding: 6px 0;
`;

const StyledCost = styled.div`
  display: flex;
  font-size: 14px;
  color: white;
`;

const Cost = ({costList}) => {
  const costMap = costList.map(({ icon_name, amount_dot }, i) => {
    return (
      <StyledCost key={i}>
        <span>{amount_dot}</span>
        <Icon name={icon_name}/>
      </StyledCost>
    )
  });
  return (
    <Container>
      {costMap}
    </Container>
  )
};

Cost.propTypes = {
  costList: PropTypes.array.isRequired
};

export default Cost;
