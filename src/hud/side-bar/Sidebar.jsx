import React, {PureComponent} from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import connectToPlayer from "../../player/connectToPlayer";
import * as colors from '../../constants/colors';
import ScreenOverview from "./screens/overview";
import ScreenLand from "./screens/land/ScreenLand";
import ScreenFood from './screens/food/ScreenFood';
import ScreenMoney from './screens/money/ScreenMoney';
import ScreenLevel from './screens/level/ScreenLevel';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 20%;
  background-color: ${colors.blue800};
  color: white;
  padding: 30px 10px;
  width: 280px;
`;

class Sidebar extends PureComponent {

  componentDidUpdate(prevProps) {
    if ( prevProps.player !== this.props.player ) {
      //   Show changes
    }
  }

  router = () => {
    const { player, turn, game } = this.props;
    const { sidebar_route } = game;
    switch(sidebar_route) {
      case 'land_overview' :
        return <ScreenLand player={player} />;
      case 'food_overview' :
        return <ScreenFood player={player} />;
      case 'money_overview' :
        return <ScreenMoney player={player} />;
      case 'level_overview' :
        return <ScreenLevel player={player} />;
      default :
        return <ScreenOverview player={player} turn={turn}/>

    }
  }

  render() {
  // const {
  //   corn_amount,
  //   corn_max,
  //   land_amount,
  //   land_max,
  //   land_free,
  //   money_amount,
  //   name,
  //   people_amount,
  //   people_max,
  //   prestige,
  //   rank_name,
  //   tax_rate,
  //   // property,
  // } = this.props.player;
  // const {
  //   city,
  //   field,
  //   market,
  //   statue,
  //   windmill
  // } = property;
  //   console.log(this.props.turn.people.need('min'))
    return (
      <Container>
        {this.router()}
        {/*<ScreenOverview player={this.props.player} turn={this.props.turn}/>*/}
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  game: state.game
});

const withPlayer = connectToPlayer(Sidebar)

export default connect(mapStateToProps)(withPlayer);
