import React from 'react';
import PropTypes from 'prop-types';
import Container from "../../Container";
import Box from "../../../styles/Box";
import StatWithIcon from "../../../elements/StatWithIcon";
import Turn from "../../../../turn/Turn";
import buildings from "../../../../constants/buildings";
import Resource from "../../../styles/Resource";
import FullStat from "../../../elements/FullStat";

const ScreenLand = ({ player }) => {
  const {
    corn_amount,
    corn_max,
  } = player;
  const turn = new Turn(player);
  return (
    <Container>
      <p className="headline">
        Nahrung
      </p>
      <Box>
        <p className="headline--small">
          Übersicht Nahrung
        </p>
        <FullStat
          icon='acorn'
          value={corn_amount}
          tooltip='Die Menge an Korn in Euren Speichern'
          description='Nahrung'
        />
        <FullStat
          icon='acorn'
          value={corn_max}
          tooltip='Die Menge Korn, die maximal in Eure Speicher passt'
          description='Nahrung Max'
          max
        />
      </Box>
      <Box
        bottomLine
        bottom={
          <StatWithIcon
            icon='acorn'
            value={turn.corn.raise_expected}
            tooltip='Die erwartete Ernte pro Runde'
          />
        }
      >
        <p className="headline--small">
          Zuwachs
        </p>
        <Resource
          name='field'
          value={turn.corn.from_fields_expected}
          tooltip={buildings.field.description}
        />
        <Resource
          name='windmill'
          value={turn.corn.from_windmills_expected}
          tooltip={buildings.windmill.description}
        />
      </Box>
    </Container>
  )
};

ScreenLand.propTypes = {
  player: PropTypes.object.isRequired
};

export default ScreenLand;
