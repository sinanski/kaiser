import React from 'react';
import PropTypes from 'prop-types';
import Container from "../../Container";
import Box from "../../../styles/Box";
import StatWithIcon from "../../../elements/StatWithIcon";
import FullStat from "../../../elements/FullStat";
import buildings from "../../../../constants/buildings";
import Resource from "../../../styles/Resource";

const ScreenLand = ({ player }) => {
  const {
    land_amount,
    land_max,
    land_free,
    property,
  } = player;
  const {
    city,
    field,
    market,
    // statue,
    windmill
  } = property;
  return (
    <Container>
      <Box>
        <p className="headline--small">
          Übersicht Land
        </p>
        <FullStat
          icon='wooden-sign'
          value={land_free}
          pie
          tooltip='Unbebautes Land'
          description='Freies Land'
        />
        <FullStat
          icon='wooden-sign'
          value={land_amount}
          tooltip='Das gesamte Land in eurem Besitz'
          description='Erworbenes Land'
        />
        <FullStat
          icon='wooden-sign'
          value={land_max}
          max
          noMargin
          tooltip='Das maximale Land das ihr besitzen dürft'
          description='Land Max'
        />
      </Box>
      <Box
        bottomLine
        bottom={
          <StatWithIcon
            icon='wooden-sign'
            value={player.land_occupied}
            tooltip='Das maximale Land das ihr besitzen dürft'
          />
        }
      >
        <Resource
          name='field'
          value={field * buildings.field.cost.land}
          amount={field}
          tooltip={buildings.field.description}
        />
        <Resource
          name='market'
          value={market * buildings.market.cost.land}
          amount={market}
          tooltip={buildings.market.description}
        />
        <Resource
          name='windmill'
          value={windmill * buildings.windmill.cost.land}
          amount={windmill}
          tooltip={buildings.windmill.description}
        />
        <Resource
          name='city'
          value={city * buildings.city.cost.land}
          amount={city}
          tooltip={buildings.city.description}
        />
      </Box>
      <Box>
        <p className="headline--small">
          Schön wäre hier noch zu sehen, wie sich der Max Amount zusammen setzt.
          Also Timeline von jeder Turn und jedem Level Aufstieg schaffen
        </p>
      </Box>
    </Container>
  )
};

ScreenLand.propTypes = {
  player: PropTypes.object.isRequired
};

export default ScreenLand
