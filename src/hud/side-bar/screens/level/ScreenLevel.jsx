import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Container from "../../Container";
import Box from "../../../styles/Box";
import Resources from "../../../../game/screens/level/Resources";

const Block = styled.div`
  margin-bottom: 20px;
`;

const ScreenLand = ({ player }) => {
  const { next, reward, shouldRaise, lacksForRaise } = player.rankUp;
  return (
    <Container>
      Rang
      { !shouldRaise &&
        <Box>
          <div>
            <Block>

            <p className='headline--small'>Benötigt</p>
            <Resources reward={next.dependencies} />
            </Block>
            <p className='headline--small'>
              Euch fehlt
            </p>
          </div>
          <Resources reward={lacksForRaise} />
        </Box>
      }

      <Box>
        <div>
          <p className="headline--small">
            Nächster Rang
          </p>
          <p>
            {next.title}
          </p>
        </div>
        <Resources reward={reward} />
      </Box>
    </Container>
  )
};

ScreenLand.propTypes = {
  player: PropTypes.object.isRequired
};

export default ScreenLand;
