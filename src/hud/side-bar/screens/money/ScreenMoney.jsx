import React from 'react';
import PropTypes from 'prop-types';
import Container from "../../Container";
import Box from "../../../styles/Box";
import StatWithIcon from "../../../elements/StatWithIcon";
import Turn from "../../../../turn/Turn";
import buildings from "../../../../constants/buildings";
import Resource from "../../../styles/Resource";
import FullStat from "../../../elements/FullStat";

const ScreenLand = ({ player }) => {
  const turn = new Turn(player);
  const income = turn.income(player);
  return (
    <Container>
      Money Overview
      <Box
      bottomLine
      bottom={
        <StatWithIcon
          icon='gold-bar'
          value={income.sources.market}
          tooltip='Geld in Euren Schatzkammern'
        />
      }
      >
        <Resource
          name='market'
          value={income.sources.market}
          tooltip={buildings.market.description}
        />
        <Resource
          name='city'
          value={income.sources.city || 0}
          tooltip={buildings.city.description}
        />
      </Box>
      <Box
        bottomLine
        bottom={
          <StatWithIcon
          icon='forging'
          value={income.sources.trade + income.sources.tax}
          tooltip='Geld aus Handel'
        />
        }
      >
        <FullStat
          icon='forging'
          value={income.sources.trade}
          tooltip='Geld aus Handel'
          description='Handel'
        />
        <FullStat
          icon='castle-flag'
          value={income.sources.tax}
          tooltip='Geld aus Steuern'
          description='Steuern'
        />
      </Box>
      <Box
        bottomLine
        bottom={
          <StatWithIcon
            icon='gold-bar'
            value={income.total}
            tooltip='Geld durch Gebäude'
          />
        }
      >
        <FullStat
          icon='gold-bar'
          value={income.sources.market + (income.sources.city || 0)}
          tooltip='Geld durch Gebäude'
          description='Gebäude'
        />
        <FullStat
          icon='gold-bar'
          value={income.sources.trade + income.sources.tax}
          tooltip='Geld durch Handel und Steuer'
          description='Handel & Steuern'
        />
      </Box>
    </Container>
  );
};

ScreenLand.propTypes = {
  player: PropTypes.object.isRequired
};

export default ScreenLand
