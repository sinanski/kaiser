import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Progress from "../../../elements/Progress";
import { StatContainer } from "./Styles";
import Icon from "../../../Icon";
import Box from "../../../styles/Box";


const Container = styled.div`
  margin-bottom: 14px;
`;

const Indicator = ({
  current,
  max,
  icon,
  indicator,
                         }) => (
  <Container>
    <StatContainer>
      <Progress
        current={current}
        max={max}
        indicator={indicator}
      />
      <Icon name={icon} />
    </StatContainer>
  </Container>
);

function getCornIndication(corn_amount, corn_max, people) {
  const medNeed = people.need('med');
  const maxNeed = people.need('max');
  const min = medNeed / corn_max * 100;
  const med = maxNeed / corn_max * 100 - min;
  return {
    min,
    med
  }
}

const Indicators = ({
  corn_amount,
  corn_max,
  land_amount,
  land_max,
  people_amount,
  people_max,
  people
                    }) => (
  <div>
    <Box shortBottom>

      <Indicator
        current={corn_amount}
        max={corn_max}
        icon='acorn'
        indicator={getCornIndication(corn_amount, corn_max, people)}
      />
      <Indicator
        current={land_amount}
        max={land_max}
        icon='wooden-sign'
      />
      <Indicator
        current={people_amount}
        max={people_max}
        icon='double-team'
      />

    </Box>
  </div>
);

Indicators.propTypes = {
  corn_amount: PropTypes.number.isRequired,
  corn_max: PropTypes.number.isRequired,
  land_amount: PropTypes.number.isRequired,
  land_max: PropTypes.number.isRequired,
  people_amount: PropTypes.number.isRequired,
  people_max: PropTypes.number.isRequired
};

export default Indicators
