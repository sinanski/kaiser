import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Box from "../../../styles/Box";
import * as colors from '../../../../constants/colors';
import FullStat from "../../../elements/FullStat";

const Container = styled.div`
  //display: flex;
`;

const Player = styled.p`
  margin-top: 0;
  margin-bottom: ${ props => props.year ? '4px' : '15px' };
  font-size: ${ props => props.year ? '10px' : '14px' };
  color: ${ props => props.year ? colors.blue200 : 'white' };
`;

const Overview = ({
  rank_name,
  player_name,
  free_land,
  corn_amount,
  money_amount,
  people_amount,
  turn
                  }) => (
  <Container>
    <Box shortBottom>
      <Player year>
        {turn.year}
      </Player>
      <Player>
        {`${rank_name} ${player_name}`}
      </Player>

      <FullStat
        value={corn_amount}
        icon='acorn'
        tooltip='Nahrungsvorräte in Euren Kornkammern'
        description='Nahrungsvorräte'
      />
      <FullStat
        value={free_land}
        icon='wooden-sign'
        tooltip='Freies Land auf dem ihr bauen könnt'
        description='Freies Land'
      />
      <FullStat
        value={people_amount}
        icon='double-team'
        tooltip='Eure derzeitigen Einwohner'
        description='Einwohner'
      />
      <FullStat
        value={money_amount}
        icon='gold-bar'
        description='Geld'
      />
    </Box>
  </Container>
);

Overview.propTypes = {
  rank_name: PropTypes.string.isRequired,
  player_name: PropTypes.string.isRequired,
  free_land: PropTypes.number.isRequired,
  corn_amount: PropTypes.number.isRequired,
  money_amount: PropTypes.number.isRequired,
  people_amount: PropTypes.number.isRequired,
};

export default Overview
