import React from 'react';
import PropTypes from 'prop-types';
import Indicators from "./Indicators";
import Box from "../../../styles/Box";
import Overview from "./Overview";
import Container from "../../Container";
import FullStat from "../../../elements/FullStat";


const ScreenOverview = ({player, turn}) => {
  const {
    corn_amount,
    corn_max,
    land_amount,
    land_max,
    land_free,
    money_amount,
    name,
    people_amount,
    people_max,
    prestige,
    rank_name,
    tax_rate,
    // property,
  } = player;
  return (
    <Container>
      <Overview
        turn={turn}
        free_land={land_free}
        player_name={name}
        corn_amount={corn_amount}
        rank_name={rank_name}
        money_amount={money_amount}
        people_amount={people_amount}
      />
      <Indicators
        land_max={land_max}
        corn_amount={corn_amount}
        land_amount={land_amount}
        people_amount={people_amount}
        people_max={people_max}
        corn_max={corn_max}
        people={turn.people}
      />
      <Box shortBottom>
        <FullStat
          value={prestige} icon='crown'
          tooltip='Zusätzliches Prestige ermöglicht Euch mehr Land zu erwerben'
          description='Prestige'
        />
        <FullStat
          value={tax_rate * 100 + '%'}
          icon='castle-flag'
          tooltip='Steuern'
          description='Steuern'
        />
      </Box>
    </Container>
  )
};

ScreenOverview.propTypes = {
  player: PropTypes.object.isRequired,
  turn: PropTypes.object.isRequired,
};

export default ScreenOverview
