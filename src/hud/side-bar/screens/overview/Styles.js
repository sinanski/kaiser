import styled from "styled-components";

export const StatContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: ${props => props.left ? 'flex-start' : 'flex-end'};
  //margin-bottom: ${props => !props.noMargin ? '14px' : 0};
  flex: 1 1 50%;
`;

export const Up = styled.div`
  position: absolute;
  right: -5px;
  top: 6px;
  font-size: 13px;
  z-index: 1;
`;

export const Name = styled.div`
  
`;
export const Value = styled.div`
  
`;

export const Image = styled.img`
  height: 20px;
  width: 20px;
  padding-left: 14px;
`;