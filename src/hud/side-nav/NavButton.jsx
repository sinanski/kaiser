import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as colors from '../../constants/colors';
import Icon from "../Icon";

const StyledButton = styled.button`
  position:relative;
  display: flex;
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
  outline: none;
  border: none;
  margin: 10px 0;
  color: white;
  //background: transparent;
  background-color: ${props => props.active ? colors.blue800 : 'transparent'};
  cursor: pointer;
  border-radius: 6px;
  transition: background-color .5s ease;
  &:hover {
    background-color: ${colors.blue800};
  }
  &:after {
    content: '';
    visibility: ${props => props.active ? 'visible' : 'hidden'};
    position: absolute;
    top: 0;
    right: -14px;
    display: block;
    width: 20px;
    height: 100%;
    background-color: inherit;
  }
`;

const NavButton = ({
  icon = 'diamonds-card',
  prefix,
  onClick,
  tooltip,
  active
                   }) => (
  <StyledButton
    onClick={onClick}
    data-tip={tooltip}
    data-place='right'
    active={active}
  >
    <Icon name={icon} noPadding size='2x' prefix={prefix} />
  </StyledButton>
);

NavButton.propTypes = {
  icon: PropTypes.string,
  prefix: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default NavButton
