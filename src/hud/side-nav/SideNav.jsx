import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import NavButton from "./NavButton";
import * as colors from '../../constants/colors';
import {routeSidebar} from "../../game/actions";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${colors.blue900};
  color: white;
  width: 80px;
  justify-content: center;
  align-items: center;
`;

class SideNav extends PureComponent {

  routeTo = route => {
    console.log('route to ', route)
    this.props.dispatch(
      routeSidebar(route)
    )
  };

  isActive = route => {
    const {sidebar_route} = this.props.game;
    return sidebar_route === route;
  }

  render() {
    return (
      <Container>
        <NavButton
          icon='podium'
          active={this.isActive('overview')}
          tooltip='Allgemeine Spielübersicht'
          onClick={() => this.routeTo('overview')}
        />
        <NavButton
          icon='acorn'
          active={this.isActive('food_overview')}
          onClick={() => this.routeTo('food_overview')}
        />
        <NavButton
          icon='wooden-sign'
          active={this.isActive('land_overview')}
          onClick={() => this.routeTo('land_overview')}
        />
        <NavButton
          icon='gold-bar'
          active={this.isActive('money_overview')}
          onClick={() => this.routeTo('money_overview')}
        />
        <NavButton
          icon='ribbon-b'
          prefix='game'
          active={this.isActive('level_overview')}
          onClick={() => this.routeTo('level_overview')}
        />
        {/*<NavButton*/}
        {/*  icon='gears'*/}
        {/*  onClick={() => this.routeTo('me')}*/}
        {/*/>*/}
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps)(SideNav)
