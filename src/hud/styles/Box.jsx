import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as colors from '../../constants/colors'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: ${props => props.grow ? '1 1 auto' : '0 1 auto'};
  background-color: ${colors.blue400};
  border-radius: 21px;
  overflow: hidden;
  margin: 12px 0;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  &:* {
  margin: 0 6px;
  }
`;

const padding = ({shortBottom, noPadding}) => {
  if ( noPadding ) return 0;
  return !shortBottom
    ? '20px 20px'
    : '20px 20px 10px'
}

const Content = styled.div`
  //padding: 20px 30px;
  min-height: 45px;
  padding: ${ props => padding(props) };

`;

const Center = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-width: 300px;
  // margin-bottom: ${ props => props.gapBottom ? '20px' : 0 };
  //padding: 7px 30px;
  //min-height: 45px;
`;

const Bottom = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: ${props => !props.fullBottom ? '0 20px' : 0};
  min-height: 40px;
  flex: 1 1 auto;
  //width: 100%;
  //background-color: red;
  border-top: ${props => props.bottomLine
    ? `1px solid ${colors.blue200}`
    : 'none'
  };
`;

const Box = ({
  children,
  bottom,
  bottomLine,
  alignEnd,
  grow,
  shortBottom,
  noPadding,
  fullBottom
}) => (
  <Container alignEnd={alignEnd} grow={grow} >
    <Content shortBottom={shortBottom} noPadding={noPadding} >
      {children}
    </Content>
    { !!bottom &&
      <Bottom
        bottomLine={ bottomLine }
        fullBottom={fullBottom}
      >
        {bottom}
      </Bottom>
    }
  </Container>
);

Box.propTypes = {
  bottomLine: PropTypes.bool,
  alignEnd: PropTypes.bool,
  grow: PropTypes.bool,
  shortBottom: PropTypes.bool,
};

Box.Center = Center;
Box.Row = Row;

export default Box
