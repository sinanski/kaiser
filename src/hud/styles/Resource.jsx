import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactTooltip from "react-tooltip";
import { fromValues } from '../../assets/utils/convertForView';
import * as colors from '../../constants/colors';

const Container = styled.div`
  position:relative;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 8px 5px;
`;

const Image = styled.img`
  width: 30px;
  height: 30px;
`;

const Value = styled.div`
  font-size: 16px;
  padding-right: 20px;
`;

const Amount = styled.div`
  position: absolute;
  right: 3px;
  bottom: 3px;
  color: ${colors.blue200};
  font-size: 12px;
`;

const Resource = ({
  name,
  amount,
  value = 0,
  tooltip
}) => {
  const result = fromValues(name, value);
  return (
    <Container data-tip={tooltip}>
      <ReactTooltip/>
      <Value>
        {result.amount_dot}
      </Value>
      <Image src={result.image} alt=""/>
      { amount !== undefined &&
        <Amount>
          {amount}
        </Amount>
      }
    </Container>
  )
};

Resource.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  amount: PropTypes.number,
  tooltip: PropTypes.string,
};

export default Resource
