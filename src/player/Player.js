import defaultPlayer from "./defaultPlayer";

export default class Player {
  constructor(playerProps) {
    const {
      name,
      index,
      rank,
      rank_name,
      people_amount,
      people_max,
      land_amount,
      land_max,
      corn_amount,
      corn_max,
      money_amount,
      prestige,
      field,
      market,
      windmill,
      city,
      statue,
      tax_rate,
      statue_max
    } = initialPlayer(playerProps)
    this.name = name;
    this.index = index;
    this.rank = rank;
    this.rank_name = rank_name;
    this.people_amount = people_amount;
    this.land_amount = land_amount;
    this.corn_amount = corn_amount;
    this.money_amount = money_amount;
    this.prestige = prestige;
    this.tax_rate = tax_rate;
    this.statue_max = statue_max;
    this.attributes = {
      corn_max,
      land_max,
      people_max
    };
    this.property = {
      field,
      market,
      windmill,
      city,
      statue
    }
  }
}

function initialPlayer(playerProps) {
  return {
    ...defaultPlayer,
    ...playerProps
  }
}
