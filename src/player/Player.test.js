import Player from "./Player";
import PlayerMethods from "./methods/PlayerMethods";

// const initialPlayer = new InitialPlayer();
const playerMethods = new PlayerMethods();

describe('Player', () => {
  test('Initial Player returns default player with alteration of given props', () => {
    const player = new Player({name: 'Testi'});
    const player2 = new Player({index: 1});
    expect(player).toHaveProperty('name', 'Testi', );
    expect(player).toHaveProperty('index', 0, );
    expect(player2).toHaveProperty('index', 1, );
  });

  test('rank up returns right object structure', () => {
    const player = new Player({name: 'Rank Test', people_amount: 1});
    const rank = playerMethods.rankUp(player);
    expect(rank).toMatchObject({
      next: expect.any(Object),
      shouldRaise: expect.any(Boolean),
      lacksForRaise: [{people_amount: 349}],
      reward: expect.any(Array),
      player_next: expect.any(Object),
    })



    // expect(rank).toEqual(
    //   expect.objectContaining({
    //     title: expect.any(String),
    //     next: expect.any(Object),
    //     shouldRaise: expect.any(Boolean),
    //     lacksForRaise: expect.any(Array),
    //   }),
    // );
  });

  test('farmedLand returns a number that equals all the players property multiplied by their land cost', () => {
    const player = new Player({
      land_amount: 10,
      field: 5,
      windmill: 0,
      market: 0
    });
    const farmed = playerMethods.farmedLand(player);
    expect(farmed).toBe(5)
  });

  test('farmedLand returns a number that equals all the players property multiplied by their land cost', () => {
    const player = new Player({
      land_amount: 10,
      money_amount: 5000,
      land_max: 11,
    });
    const player2 = new Player({
      land_amount: 10,
      money_amount: 5000,
      land_max: 10,
    });
    const canAfford = playerMethods.canAffordLand(player);
    const canAfford2 = playerMethods.canAffordLand(player2);
    expect(canAfford).toBe(true)
    expect(canAfford2).toBe(false)
  });

  test('updatePlayer recieves the old player and the object with changes and returns the new player', () => {
    // Does not work for property
    const player = new Player({
      land_amount: 10,
      money_amount: 5,
      corn_amount: 50,
    });
    const changes = {land_amount: 3, money_amount: 50}
    const newPlayer = playerMethods.updatePlayer(player, changes);
    expect(newPlayer).toHaveProperty('land_amount', 13);
    expect(newPlayer).toHaveProperty('money_amount', 55);
    expect(newPlayer).toHaveProperty('corn_amount', 50);
  });

  test('cornMax returns the maximum amount for corn. Dynamically calculated', () => {
    const player = new Player({ windmill: 2, corn_max: 500 })
    const player2 = new Player({ windmill: 3, corn_max: 0 })
    const corn_max = playerMethods.cornMax(player);
    const corn_max2 = playerMethods.cornMax(player2);
    expect(corn_max).toBe(800);
    expect(corn_max2).toBe(450);
  })
});
