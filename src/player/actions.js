export function setPlayers(players) {
  return {
    type: 'SET_PLAYERS',
    players
  };
}

export function updatePlayer(player, i) {
  return {
    type: 'UPDATE_PLAYER',
    player,
    i
  };
}

export function storeProvidedFoodRatio(min_med_max) {
  return {
    type: 'STORE_PROVIDED_FOOD_RATIO',
    min_med_max
  };
}

export function resetProvideFood(min_med_max) {
  return {
    type: 'RESET_PROVIDE_FOOD',
    min_med_max
  };
}
