import React from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { updatePlayer } from "./actions";
import PlayerMethods from "./methods/PlayerMethods";

const methods = new PlayerMethods();

function connectToPlayer(WrappedComponent) {
  return class extends React.Component {

    updatePlayer = player => {
      const { dispatch, active } = this.props;
      dispatch(updatePlayer(player, active))
    };

    changePlayer = change => {
      const newPlayer = methods.updatePlayer(this.props.player, change);
      this.updatePlayer(newPlayer)
    };

    canBuyLand = price => {
      return methods.canAffordLand(this.props.player, price);
    };

    canSellLand = () => {
      return methods.canSellLand(this.props.player);
    };

    buyLand = price => {
      const newPlayer = methods.buyLand(this.props.player, price);
      this.updatePlayer(newPlayer)
    };

    sellLand = price => {
      const newPlayer = methods.sellLand(this.props.player, price);
      this.updatePlayer(newPlayer)
    };

    // cornMax = () => {
    //   return methods.cornMax(this.props.player)
    // };
    //
    // landMax = () => {
    //   return methods.cornMax(this.props.player)
    // };

    canBuyCorn = price => {
      return methods.canAffordCorn(this.props.player, price);
    };

    canSellCorn = ( amount = 1 ) => {
      return methods.canSellCorn(this.props.player, amount);
    };

    buyCorn = (price, amount) => {
      const newPlayer = methods.buyCorn(this.props.player, price, amount);
      this.updatePlayer(newPlayer)
    };

    sellCorn = (price, amount) => {
      const newPlayer = methods.sellCorn(this.props.player, price, amount);
      this.updatePlayer(newPlayer)
    };

    foodDemand = min_med_max => {
      const { player, provided_food } = this.props;
      const chosen = min_med_max || provided_food;
      return methods.foodDemand(player, chosen)
    };

    provideFood = min_med_max => {
      const { player, provided_food } = this.props;
      const chosen = min_med_max || provided_food;
      return methods.provideFood(player, chosen)
    };

    currentPlayer = () => {
      const { player } = this.props;
      return {
        ...player,
        corn_max: methods.cornMax(player),
        land_max: methods.landMax(player),
        people_max: methods.peopleMax(player),
        land_free: methods.freeLand(player),
        land_occupied: methods.farmedLand(player),
        rankUp: methods.rankUp(player)
      }
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          player={this.currentPlayer()}
          updatePlayer={ this.updatePlayer }
          changePlayer={this.changePlayer}
          canBuyCorn={this.canBuyCorn}
          canSellCorn={this.canSellCorn}
          buyCorn={this.buyCorn}
          sellCorn={this.sellCorn}
          canBuyLand={this.canBuyLand}
          canSellLand={this.canSellLand}
          buyLand={this.buyLand}
          sellLand={this.sellLand}
          foodDemand={this.foodDemand}
          provideFood={this.provideFood}
        />
      );
    }
  };
}

const mapStateToProps = state => ({
  provided_food: state.players.provided_food,
  active: state.players.active,
  player: state.players.listOfPlayers[state.players.active]
});

export default compose(
  connect(mapStateToProps),
  connectToPlayer
)
