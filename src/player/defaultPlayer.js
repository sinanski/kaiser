const defaultPlayer = {
  name: '',
  index: 0,
  rank: 0,
  rank_name: 'Herr',
  round: 0,
  people_amount: 300,
  people_max: 2500,
  land_amount: 25,
  land_max: 50,
  corn_amount: 350,
  corn_max: 1000,
  money_amount: 15000,
  prestige: 0,
  field: 10,
  market: 5,
  windmill: 2,
  city: 0,
  tax_rate: .05,
  // cities_parts: 0,
  statue: 0,
  statue_max: 0
};

export default defaultPlayer;