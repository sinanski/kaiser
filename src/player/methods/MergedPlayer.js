import Player from "../Player";
import PlayerMethods from "./PlayerMethods";

export default function MergedPlayer(props, ranks) {
  return {
    ...new Player(props),
    ...new PlayerMethods(ranks)
  }
};
