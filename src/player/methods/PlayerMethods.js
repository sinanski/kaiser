import compare from "../../assets/utils/compare";
import buildings from '../../constants/buildings'
import { updatePlayer } from "../update";
import People from "../../turn/people";
import RankedPlayer from "./RankedPlayer";
import * as max from './max';

const sellPrice = .8;

export default function PlayerMethods(providedRanks) {

  // Ranks are only assignable for testing purpose
  this.rankUp = player => {
    return new RankedPlayer(player, providedRanks)
  };

  this.farmedLand = player => {
    let totalAmount = 0;
    for ( let i in player.property ) {
      const amount = player.property[i];
      const farmedLand = buildings[i].cost.land_amount;
      if ( amount ) {
        totalAmount += amount * farmedLand
      }
    }
    return totalAmount
  };

  this.freeLand = player => {
    const farmed_land = this.farmedLand(player)
    return player.land_amount - farmed_land
  };

  this.canAfford = (player, price, type, raiseBy = 1) => {
    const isExceeded = max.isExceeded(player, price, type, raiseBy);
    const dependency = { money_amount: price };
    const lacks = compare.getName(dependency, player);
    return !lacks.length && !isExceeded;
  };

  this.canAffordLand = (player, price) => {
    return this.canAfford(player, price, 'land')
  };

  this.buyLand = (player, price) => {
    const canAfford = this.canAffordLand(player, price);
    if ( canAfford ) {
      return this.updatePlayer(player, {
        land_amount: 1,
        money_amount: price * -1
      })
    }
    return player
  };

  this.canSellLand = player => {
    return this.freeLand(player) > 0;
  };


  this.canSellCorn = (player, amount) => {
    return player.corn_amount >= amount;
  };

  this.sellLand = (player, price) => {
    if ( this.canSellLand(player) ) {
      return this.updatePlayer(player, {
        land_amount: -1,
        money_amount: price * sellPrice
      })
    }
  };

  this.canAffordCorn = (player, price, raiseBy = 100) => {
    return this.canAfford(player, price, 'corn', raiseBy)
  };

  this.buyCorn = (player, price, raiseBy) => {
    const canAfford = this.canAffordCorn(player, price, raiseBy);
    if ( canAfford ) {
      return this.updatePlayer(player, {
        corn_amount: raiseBy,
        money_amount: price * raiseBy * -1
      })
    }
    return player
  };

  this.sellCorn = (player, price, amount) => {
    if ( this.canSellCorn(player, amount) ) {
      return this.updatePlayer(player, {
        corn_amount: amount * -1,
        money_amount: price * amount * sellPrice
      })
    }
    return player
  };

  this.updatePlayer = ( player, change, amount = 1 ) => {
    return updatePlayer(player, change, amount)
  };

  this.foodDemand = (player, min_med_max) => {
    const people = new People(player);
    const amount = people.need(min_med_max);
    return {
      amount,
      isMoreThanAvailable: amount > player.corn_amount,
      isMoreThanMax: amount > this.cornMax(player)
    }
  };

  this.provideFood = (player, min_med_max) => {
    const people = new People(player);
    return people.provideFood(min_med_max);
  };

  this.cornMax = player => {
    return max.corn(player)
  };

  this.landMax = player => {
    return max.land(player);
  };

  this.peopleMax = player => {
    return max.people(player)
  }

}
