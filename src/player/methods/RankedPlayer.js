import ranks from "../ranks";
import compare from "../../assets/utils/compare";
import Reward from "./Reward";

// todo city is not checked

export default function RankedPlayer(player, providedRanks) {
  this.ranks = providedRanks || ranks;
  const next = getNextRank(player, this.ranks);
  const dependencies = next.dependencies || {};
  const doesRank = compare.getMissing(dependencies, player);
  const rewardList = objectToArray(next.reward);
  const following = getFollowingRank(player, this.ranks);
  const reward = {
    ...next.reward,
    rank_name: next.title,
    rank: 1,
  };
  const player_next = new Reward(player, reward).next;
  this.next = new Conversion(next);
  this.shouldRaise = !doesRank.length;
  this.lacksForRaise = doesRank;
  this.reward = rewardList;
  this.player_next = player_next;
  this.following = new Conversion(following)
}

function Conversion(level) {
  this.title = level.title;
  this.reward = objectToArray(level.reward);
  this.dependencies = objectToArray(level.dependencies);
}

function getNextRank(player, ranks) {
  const i = player.rank + 1;
  return ranks[i] || {}
}

function getFollowingRank(player, ranks) {
  const i = player.rank + 2;
  return ranks[i] || {}
}

function objectToArray(object) {
  let list = [];
  for ( let i in object ) {
    list.push({
      [i]: object[i]
    })
  }
  return list;
}
