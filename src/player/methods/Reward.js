
export default function Reward(player, reward) {
  this.reward = reward;
  this.next = playerAfterReward(player, reward);
};


function playerAfterReward(player, reward) {
  let newPlayer = player;
  for ( let i in player ) {
    if ( reward[i] ) {
      newPlayer = updateStat(newPlayer, reward, i)
    }
    if ( isObject(player[i]) ) {
      newPlayer = updateSubStat(newPlayer, reward, i)
    }
  }
  return newPlayer;
}

function updateSubStat(player, reward, key) {
  return {
    ...player,
    [key]: playerAfterReward(player[key], reward)
  }
}

function isObject(key) {
  return typeof key === 'object'
}

function updateStat(playerStat, reward, key) {
  const next = newAmount(playerStat, reward, key);
  return {
    ...playerStat,
    [key]: next
  };
}

function newAmount(playerStat, reward, key) {
  const oldAmount = playerStat[key] || 0;
  const change = reward[key];
  const isChangeName = typeof change === 'string';
  return isChangeName
    ? change
    : oldAmount + change;
}
