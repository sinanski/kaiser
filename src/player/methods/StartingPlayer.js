import Player from "../Player";
import config from "../../config";

const { starting_player_config } = config;


export default function StartingPlayer() {
  // TODO Add the possibility to provide rank. While Starting player loops through rewards until this rank.
  return new Player(starting_player_config)
};
