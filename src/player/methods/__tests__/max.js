import * as max from '../max';
import MergedPlayer from "../MergedPlayer";
import windmill from "../../../constants/buildings/windmill";
import city from "../../../constants/buildings/city";

const windmill_amount = 2;
const player_corn_max = 1000;
const player_people_max = 2500;

const player = new MergedPlayer({
  corn_max: player_corn_max,
  land_max: 50,
  people_max: player_people_max,
  windmill: windmill_amount,
  city: 1,
});

describe('Getting max amounts', () => {

  test('max_corn', () => {
    const cornMax = player.cornMax(player);
    const fromBuildings = windmill.reward.corn_max * windmill_amount;
    const expected = fromBuildings + player_corn_max;
    expect(cornMax).toBe(expected)
  });

  test('people_max', () => {
    const peopleMax = player.peopleMax(player);
    const fromBuildings = city.reward.people_max;
    const expected = fromBuildings + player_people_max;
    expect(peopleMax).toBe(expected)
  });

});
