import MergedPlayer from "../MergedPlayer";
// import ranks from "../../ranks";

const ranks = [
  {
    title: 'Herr',
  },
  {
    title: 'Bürger',
    dependencies: {
      people_amount: 350,
      money_amount: 5000
    },
    reward: {
      money_amount: 5000,
      land_max: 10
    }
  },
];

describe('rank', () => {
  test('returns needed object structure', () => {
    const player = new MergedPlayer();
    const rank = player.rankUp(player);
    expect(rank).toEqual(
      expect.objectContaining({
        next: expect.any(Object),
        shouldRaise: expect.any(Boolean),
        lacksForRaise: expect.any(Array),
        reward: expect.any(Array),
      })
    )
  });

  test('shouldRaise is false when missing dependencies', () => {
    const player = new MergedPlayer({ people_amount: 1 });
    const rank = player.rankUp(player);
    expect(rank).toHaveProperty('shouldRaise', false)
  });

  test('shouldRaise is true when dependencies fit', () => {
    const player = new MergedPlayer({ people_amount: 1000 });
    const rank = player.rankUp(player);
    expect(rank).toHaveProperty('shouldRaise', true)
  });

  test('player is updated correctly when ranking up', () => {
    // TODO The raise of _max values through ranks need to raise player.attributes... instead
    const player = new MergedPlayer({ people_amount: 1000 });
    const rank = player.rankUp(player);
    const { money_amount, land_max } = ranks[1].reward;
    const expected = {
      ...player,
      rank_name: ranks[1].title,
      rank: 1,
      money_amount: player.money_amount + money_amount,
      attributes: {
        ...player.attributes,
        land_max: player.attributes.land_max + land_max
      }
    };
    // console.log(rank)
    // expect(rank.player_next).toMatchObject(expected)
  });
})