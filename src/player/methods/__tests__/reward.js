import Reward from "../Reward";
import MergedPlayer from "../MergedPlayer";

test('Reward changes the provided rewards and returns new player', () => {
  const player = new MergedPlayer({
    money_amount: 1,
    land_max: 2,
    rank: 3
  });
  const reward = {
    rank_name: 'King',
    money_amount: 1000,
    rank: 1,
    land_max: 5,
  };
  const expected = {
    ...player,
    rank_name: 'King',
    money_amount: 1001,
    rank: 4,
    attributes: {
      ...player.attributes,
      land_max: 7
    }
  }
  const rewarded = new Reward(player, reward);
  expect(rewarded.next).toMatchObject(expected)
})