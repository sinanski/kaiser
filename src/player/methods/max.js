import {get} from "lodash";
import buildings from "../../constants/buildings";

export function corn(player) {
  const fromBuildings = buildingsRaisingMax(player, 'corn_max');
  const fromPlayerAttributes = player.attributes.corn_max;
  return fromPlayerAttributes + fromBuildings;
}

function buildingsRaisingMax(player, key) {
  let totalAmount = 0;
  for ( let i in player.property ) {
    const amount = player.property[i];
    const reward = get(buildings[i], 'reward.' + key, 0);
    totalAmount += reward * amount
  }
  return totalAmount;
}

export function land(player) {
  return player.attributes.land_max;
}

export function people(player) {
  const fromBuildings = buildingsRaisingMax(player, 'people_max');
  const fromPlayerAttributes = player.attributes.people_max;
  return fromPlayerAttributes + fromBuildings;
}

export function isExceeded(player, price, type, raiseBy) {
  const amount = player[type + '_amount'];
  let max = Infinity;
  switch(type) {
    case 'corn':
      max = corn(player);
      break;
    case 'land':
      max = land(player);
      break;
    case 'people' :
      max = people(player);
      break;
    default :
  }
  return amount + raiseBy > max;
}
