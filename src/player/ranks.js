const ranks = [
  {
    title: 'Herr',
  },
  {
    title: 'Bürger',
    dependencies: {
      people_amount: 350,
      money_amount: 5000
    },
    reward: {
      money_amount: 5000,
      land_max: 10
    }
  },
  {
    title: 'Edelmann',
    dependencies: {
      people_amount: 500,
      land_amount: 40
    },
    reward: {
      money_amount: 10000,
      land_max: 50
    }
  },
  {
    title: 'Baron',
    dependencies: {
      people_amount: 1000,
      land_amount: 75
    },
    reward: {
      money_amount: 20000,
      prestige: 7,
      land_max: 100
    }
  },
  {
    title: 'Graf',
    dependencies: {
      city: 1,
      land_amount: 40
    },
    reward: {
      money_amount: 50000,
      prestige: 15
    }
  },
  {
    title: 'Fürst',
    dependencies: {
      people_amount: 20000,
      prestige: 15,
      land_amount: 40
    },
    reward: {
      money_amount: 100000,
      prestige: 15
    }
  },
];

export default ranks;
