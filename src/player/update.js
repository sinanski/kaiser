export function updatePlayer(player, change, multiplier) {
  let newPlayer = player;
  for ( let key in change ) {
    // console.log(key, change)
    const newAmount = getNewAmount(player, change, key, multiplier)
    newPlayer = {
      ...newPlayer,
      [key]: newAmount
    };
  }
  return newPlayer;
}

function getNewAmount(player, change, key, multiplier = 1) {
  const oldAmount = player[key] || 0;
  const amountToChange = change[key] || 0;
  // console.log('oldAmount', oldAmount)
  // console.log('amountToChange', amountToChange)
  return oldAmount + amountToChange * multiplier;
}
