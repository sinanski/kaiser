import config from '../config';

const initialState = {
  isProgressDisabled: false,
  sidebar_route: config.sidebar_route || 'overview'
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'DISABLE_PROGRESS':
      return {...state,
        isProgressDisabled: true
      };
    case 'ENABLE_PROGRESS':
      return {...state,
        isProgressDisabled: false
      };
    case 'ROUTE_SIDEBAR':
      return {...state,
        sidebar_route: action.route
      };

    default :
      return state;
  }
}
