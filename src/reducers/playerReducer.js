import config from '../config';
import StartingPlayer from "../player/methods/StartingPlayer";

const { instant_start } = config;

const testPlayer = [ new StartingPlayer() ]

const initialState = {
  active: 0,
  provided_food: '',
  listOfPlayers: instant_start
    ? testPlayer
    : []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_PLAYERS':
      return {...state,
        listOfPlayers: action.players
      };
    case 'CHANGE_ACTIVE_PLAYER':
      return {...state,
        active: action.i
      };
    case 'UPDATE_PLAYER':
      return {...state,
        listOfPlayers: [
          ...state.listOfPlayers.slice(0, action.i),
          action.player,
          ...state.listOfPlayers.slice(action.i + 1)
        ]
      };
    case 'STORE_PROVIDED_FOOD_RATIO':
      return {...state,
        provided_food: action.min_med_max
      };
    case 'RESET_PROVIDE_FOOD':
      return {...state,
        provided_food: ''
      };

    default :
      return state;
  }
}
