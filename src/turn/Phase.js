import config from '../config';
import { phases } from './phases';

// const steps = [
//   "harvest",
//   "buy",
//   "feed",
//   "change",
//   "build",
//   "income",
//   "rank"
// ];

export default class Phase {
  constructor() {
    // eslint-disable-next-line no-func-assign
    getNext = getNext.bind(this);
    this.steps = phases;
    this.start = config.starting_phase || this.steps[0];
  }
  reStart() {
    return this.steps[0];
  }
  next(currentName) {
    const i = getNext(currentName);
    return this.steps[i];
  }
  isLast(currentName) {
    const i = getNext(currentName);
    return i === this.steps.length;
  }
}

function getNext(currentName) {
  return this.steps.indexOf(currentName) + 1;
}