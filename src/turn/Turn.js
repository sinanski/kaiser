import Weather from "./weather";
import Corn from "./corn";
import People from "./people";
import Money from './money';
import Price from './price';

export default class Turn {
  constructor(player, index = 1) {
    // TODO This way the turn is allways in old state
    // the single states need to be methods that get the current player as argument
    this.index = index;
    this.year = 1700 + index;
    this.weather = new Weather();
    this.corn = new Corn(player, this.weather);
    this.people = new People(player);
    // this.money = new Money(player)
    this.price = new Price(player, this.weather)
    window.turn = this;
  }

  income(player) {
    const money = new Money(player);
    const people = new People(player);
    return money.income(people)
  }

}
