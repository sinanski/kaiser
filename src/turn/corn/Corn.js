import fieldObject from '../../constants/buildings/field';
import windmillObject from '../../constants/buildings/windmill';
import * as update from '../../assets/utils/update';
import PlayerMethods from "../../player/methods/PlayerMethods";

const cornFromFields = fieldObject.produces.corn_amount;
const cornFromWindmills = windmillObject.produces.corn_amount;
const playerMethods = new PlayerMethods();

export default class Corn {
  constructor(player, weather) {
    const {
      corn_amount,
      // corn_max,
      property
    } = player;
    const {
      field,
      windmill,
    } = property;
    const corn_max = playerMethods.cornMax(player)
    const from_fields = Math.round(field * cornFromFields * weather.factor);
    const from_windmills = Math.round(windmill * cornFromWindmills * weather.factor);
    const total = from_fields + from_windmills;
    const rotten = Math.round(corn_amount * weather.destroyed);
    const produced = update.amount(total, corn_amount, corn_max);
    const raise = produced - rotten;
    const next = corn_amount + raise;
    this.name = 'Korn';
    this.description = 'Ihr benötigt Korn für eure Einwohner. Ihr könnt Korn auf Feldern und in Mühlen erwirtschaften, oder es am Anfang eurer Runde kaufen.';
    this.from_fields = from_fields;
    this.from_fields_expected = field * cornFromFields;
    this.from_windmills = from_windmills;
    this.from_windmills_expected = windmill * cornFromWindmills;
    this.isLimitExceeded = next > corn_max;
    this.overflow = next - corn_max > 0 ? corn_max - next : 0;
    this.current = corn_amount;
    this.produced = produced;
    this.raise = raise;
    this.raise_expected = this.from_fields_expected + this.from_windmills_expected;
    this.rotten = rotten;
    this.next = next;
    this.max = corn_max;
  }

}
