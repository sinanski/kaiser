import Player from "../../player/Player";
import Weather from "../weather/Weather";
import Corn from "./Corn";
import field from "../../constants/buildings/field";
import windmill from "../../constants/buildings/windmill";
import { weather_great } from '../weather/Weather';

const corn_from_windmill = windmill.produces.corn_amount;
const corn_from_field = field.produces.corn_amount;

describe('Corn', () => {

  test('returns the expected change object', () => {
    const player = new Player();
    const weather = new  Weather();
    const corn = new Corn(player, weather);

    expect(corn).toEqual(
      expect.objectContaining({
        from_fields: expect.any(Number),
        from_fields_expected: expect.any(Number),
        from_windmills: expect.any(Number),
        from_windmills_expected: expect.any(Number),
        current: expect.any(Number),
        isLimitExceeded: expect.any(Boolean),
        overflow: expect.any(Number),
        rotten: expect.any(Number),
        produced: expect.any(Number),
        raise: expect.any(Number),
        next: expect.any(Number),
        max: expect.any(Number),
      })
    );
  });

  const field = 10;
  const windmill = 10;
  const player = new Player({field, windmill});

  test('returns values matching the multiple of producing buildings', () => {
    const weather = new  Weather();
    const corn = new Corn(player, weather);
    expect(corn.from_fields_expected).toBe(field * corn_from_field);
    expect(corn.from_windmills_expected).toBe(windmill * corn_from_windmill);
  });

  test('returns values matching the multiple of producing buildings', () => {
    const weather = new  Weather(.9);
    const corn = new Corn(player, weather);
    const weather_factor = weather_great.factor;
    const expectedFromFields = field * corn_from_field * weather_factor;
    const expectedFromWindmills = windmill * corn_from_windmill * weather_factor;
    expect(corn.from_fields).toBe(expectedFromFields);
    expect(corn.from_windmills).toBe(expectedFromWindmills);
  });

  test('the rotten amount equals the current amount * weather.destroyed', () => {
    const weather = new  Weather(.3);
    const corn = new Corn(player, weather);
    const expected = corn.current * weather.destroyed;
    expect(corn.rotten).toBe(expected)
  });

});
