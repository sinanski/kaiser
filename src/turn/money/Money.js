import buildings from '../../constants/buildings';

export default class Money {
  constructor(player) {
    this.player = player;
    this.money_at_turn_start = player.money_amount;
    this.producers = player.property;
    this.tax_rate = player.tax_rate;
  }

  income(people) {
    return new Income(this.producers, people, this.tax_rate)
  }

  spentThisTurn(money) {
    return this.money_at_turn_start - money;
  }
}

function Income(producers, people, tax_rate) {
  const income = new SourcesOfIncome(producers, people, tax_rate);
  this.total = getTotal(income.list);
  this.sources = divideSources(income.list);
}

function divideSources(incomeList) {
  let sortedIncome = {};
  incomeList.forEach(({ from, money }) => {
    sortedIncome[from] = money
  });
  return sortedIncome
}

function getTotal(incomeList) {
  const amount_list = incomeList.map(({ money }) => ( money ));
  return amount_list.reduce((a, b) => a + b, 0);

}

function SourcesOfIncome(producers, people, tax_rate) {
  this.producers = getIncomeByProducer(producers);
  this.tax = [ calcTax(people, tax_rate) ];
  this.trade = [ marketTrade(this.producers, people) ];
  this.list = []
    .concat(this.producers)
    .concat(this.tax)
    .concat(this.trade)
}

function Resource(money, from) {
  this.money = money;
  this.from = from;
}

function calcTax(people, tax_rate) {
  const amount = people.amount || people.current;
  const money = Math.round( amount * tax_rate / 10 );
  return new Resource(money, 'tax')
}

function marketTrade(fromProducers, people) {
  const market = fromProducers.filter(({ from }) => ( from === 'market' ))[0];
  return calcMarketTrade(market.money, people)
}

function calcMarketTrade(marketMoney, people) {
  const peopleFactor = getPeopleFactor(people)
  const money = Math.round( marketMoney * peopleFactor );
  return new Resource(money, 'trade')
}

function getIncomeByProducer(property) {
  let result = [];
  for ( let key in property ) {
    const { produces } = buildings[key];
    const amount_of_building = property[key];
    const income = getIncome(produces, amount_of_building);
    if ( amount_of_building ) {
      const singleResource = getSingleResource(income, key);
      result = result.concat(singleResource)
    }
  }
  return result
}

function getSingleResource(income, key) {
  let result = [];
  for ( let i in income ) {
    if ( i === 'money_amount' ) {
      result.push(new Resource(income[i], key))
    }
  }
  return result;
}

function getIncome(produces, amount_of_building) {
  let income = {};
  for ( let i in produces ) {
    const prevAmount = income[i] || 0;
    const newAmount = prevAmount + produces[i] * amount_of_building;
    income = mergeResult(income, i, newAmount);
  }
  return income
}

function getPeopleFactor(people) {
  let peopleFactor = (people.amount || people.current) / 10000;
  return peopleFactor <= 0.5
    ? peopleFactor
    : .5;
}

function mergeResult(result, key, newValue) {
  return {
    ...result,
    [key]: newValue
  }
}
