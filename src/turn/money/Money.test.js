import Player from "../../player/Player";
import People from "../people";
import Money from "./Money";

describe('Money', () => {
  const player = new Player();
  const people = new People(player)
  const money = new Money(player);

  test(' is expected object', () => {
    expect(money).toEqual(
      expect.objectContaining({
        money_at_turn_start: expect.any(Number),
        producers: expect.any(Object),
        tax_rate: expect.any(Number),
      })
    )
  });

  test(' .income returns an object with a total and an object containing all sources with their amount', () => {
    expect(money.income(people)).toEqual(
      expect.objectContaining({
        total: expect.any(Number),
        sources: expect.objectContaining({
          market: expect.any(Number),
          tax: expect.any(Number),
          trade: expect.any(Number)
        })
      })
    )
  })
});
