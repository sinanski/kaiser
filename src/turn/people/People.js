import peopleRatio from "./peopleRatio";
import * as random from '../../assets/utils/random';
import * as update from '../../assets/utils/update';
import PlayerMethods from "../../player/methods/PlayerMethods";

const methods = new PlayerMethods();

export default class People {
  constructor(player) {
    // getPeopleChange = getPeopleChange.bind(this)
    this.name = 'Einwohner';
    this.description = 'Einwohner sind das Herz eures Reiches. Sie bringen Geld auf Märkten und sind nötig um im Rang aufzusteigen. Aber eure Einwohner benötigen Korn, das Ihr auf Feldern und in Mühlen erwirtschaften könnt. Achtet darauf immer genügend davon zu besitzen, sonst müsst Ihr das nötige Korn für eure Einwohner teuer einkaufen.'
    this.current = player.people_amount;
    // this.max = player.people_max;
    this.max = methods.peopleMax(player);
    this.foodNeeded = {
      min: .5,
      med: 1,
      max: 1.8,
    };
  }

  provideFood(min_med_max) {
    // expects either 'min' || 'med || 'max' as value
    const change = new PeopleIncrement(min_med_max, this);
    // this.current = change.next;
    return change
  }

  need(min_med_max = 'med') {
    return Math.round(this.current * this.foodNeeded[min_med_max])
  }

}

function PeopleIncrement(min_med_max, people) {
  const { current, max } = people;
  const change = new Change(people.current, min_med_max);
  const birth = change.get('birth');
  const death = change.get('death');
  const imigrants = change.get('imigrants');
  const raise_expected = birth + imigrants - death;
  const raise = update.amount(raise_expected, current, max, Infinity * -1);
  this.birth = birth;
  this.death = death;
  this.imigrants = imigrants;
  this.raise_expected = raise_expected;
  this.isLimitExceeded = raise < raise_expected;
  this.overflow = raise_expected - raise;
  this.raise = getRaise(current, raise, max);
  this.current = current;
  this.next = current + this.raise;
  // this.next = getNextAmount(current, this.raise, max);
  this.max = max;
}

function Change(peopleAmount, min_med_max ) {
  this.min_med_max = min_med_max;
  this.peopleAmount = peopleAmount;
  this.get = key => {
    const typeOfChange = peopleRatio[key];
    const changingRange = typeOfChange[this.min_med_max] || {};
    const { min, max } = changingRange;
    return calcChange(min, max, this.peopleAmount)
  }
}

function calcChange(min, max, currentAmountPeople) {
  const change = random.raise(min, max) / 100;
  return Math.round(currentAmountPeople * change)
}

// function getNextAmount(thisAmount, raise, max) {
//   const newAmount = thisAmount + raise;
//   return newAmount < max
//     ? newAmount
//     : max
// }

function getRaise(current, raise, max) {
  const result = current + raise;
  return result < max
    ? raise
    : max - current
};

