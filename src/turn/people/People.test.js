import Player from "../../player/Player";
import People from "./People";
import peopleRatio from "./peopleRatio";

const player = new Player();
const people = new People(player);

describe('People', () => {
  test('provide an amount, a max and an object of the needed food {min, med, max}', () => {
    expect(people).toEqual(
      expect.objectContaining({
        current: expect.any(Number),
        max: expect.any(Number),
        foodNeeded: expect.objectContaining({
          min: expect.any(Number),
          med: expect.any(Number),
          max: expect.any(Number)
        })
      })
    );
  });

  test('.storeProvidedFoodRatio returns the change of people within a turn', () => {
    const player = new Player({people_amount: 10000});
    const people = new People(player);
    expect(people.provideFood('max')).toEqual(
      expect.objectContaining({
        birth: expect.any(Number),
        death: expect.any(Number),
        imigrants: expect.any(Number),
        raise: expect.any(Number),
        raise_expected: expect.any(Number),
        isLimitExceeded: expect.any(Boolean),
        overflow: expect.any(Number),
        current: expect.any(Number),
        next: expect.any(Number),
        max: expect.any(Number),
      })
    );
  });

  // test('.storeProvidedFoodRatio raises the amount within the people object', () => {
  //   const player = new Player();
  //   const people = new People(player);
  //   const old_amount_of_people = people.current;
  //   people.storeProvidedFoodRatio('max')
  //   const new_amount_of_people = people.current;
  //   expect(new_amount_of_people).toBeGreaterThan(old_amount_of_people)
  // })

  test('raise is in range', () => {
    const { min, max } = peopleRatio.birth.min;
    const addedPeople = people.provideFood('min');
    const peopleAmount = addedPeople.current;
    const minRaise = peopleAmount * (min / 100);
    const maxRaise = peopleAmount * (max / 100);
    expect(addedPeople.birth).toBeGreaterThanOrEqual(minRaise)
    expect(addedPeople.birth).toBeLessThanOrEqual(maxRaise)
  })

  test('do not exceed maximum', () => {
    const player = new Player({ people_amount: 2499 });
    const people = new People(player);
    const addedPeople = people.provideFood('max');
    expect(addedPeople.next).toBeLessThanOrEqual(addedPeople.max)
  })

  test('.need returns the amount of corn needed by people (can have min || med || max as parameter)', () => {
    const people_amount = Math.round(Math.random() * 2000);
    const player = new Player({people_amount});
    const people = new People(player);
    const need = people.need('med');
    const needHigh = people.need('max');
    const expected_high = people_amount * people.foodNeeded.max;
    expect(need).toBe(people_amount);
    expect(needHigh).toBeCloseTo(expected_high, 0);
  })

});
