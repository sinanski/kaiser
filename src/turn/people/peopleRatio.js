const peopleRatio = {
  birth: {
    min: {
      min: 0,
      max: 3,
    },
    med: {
      min: 1,
      max: 8,
    },
    max: {
      min: 2,
      max: 12,
    },
  },
  death: {
    min: {
      min: 2,
      max: 5,
    },
    med: {
      min: 1,
      max: 4,
    },
    max: {
      min: 1,
      max: 3,
    },
  },
  imigrants: {
    min: {
      min: null,
      max: null,
    },
    med: {
      min: null,
      max: null,
    },
    max: {
      min: 1,
      max: 10,
    },
  },
};

export default peopleRatio;