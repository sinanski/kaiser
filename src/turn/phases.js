export const phases = [
  'start',
  'corn_and_land',
  'birth',
  'build',
  'level'
];

// export const phases = [
//   "harvest",
//   "buy",
//   "feed",
//   "change",
//   "build",
//   "income",
//   "rank"
// ];

export function isLastPhase(phase) {
  const i = phases.indexOf(phase) + 1;
  return i === !!phases.length;
}

export function nextPhase(phase) {
  const i = phases.indexOf(phase) + 1
  return phases[i];
}

export function current() {
  return phases[0];
}