export default function Weather(random = Math.random()) {
  const result = weatherResult(random);
  this.text = result.text;
  this.factor = result.factor;
  this.destroyed = result.destroyed;
  this.icon_name = result.icon_name;
  this.random = random;
}

export const weather_great = {
  text: 'Tolles Wetter, Rekordernte!',
  factor: 1.5,
  destroyed: 0,
  // icon_name: 'sunbeams',
  icon_name: 'sunny',
};

const weather_good = {
  text: 'Gutes Wetter, reiche Ernte',
  factor: 1,
  destroyed: 0,
  // icon_name: 'splash'
  icon_name: 'partlysunny'
};

const weather_bad = {
  text: 'Schlechtes Wetter, wenig Ernte',
  factor: .75,
  destroyed: .1,
  // icon_name: 'poison-cloud'
  icon_name: 'cloudy'
};

const weather_catastrophic = {
  text: 'Schwere Stürme haben große Teile der Ernte vernichtet',
  factor: .35,
  destroyed: .2,
  // icon_name: 'lightning-storm'
  icon_name: 'thunderstorm'
};

function weatherResult(value) {
  switch(true) {
    case(value >= .85):
      return weather_great;
    case(value >= .5):
      return weather_good;
    case(value >= .05):
      return weather_bad;
    case(value < .05):
      return weather_catastrophic;
    default :
  }
}
