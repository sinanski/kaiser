import Weather from "./Weather";

describe('Weather', () => {

  test('returns object with text and values', () => {
    const weather = new Weather();
    expect(weather).toEqual(
      expect.objectContaining({
        text: expect.any(String),
        factor: expect.any(Number),
        destroyed: expect.any(Number),
      })
    )
  });

  test('is bad when random value equals <.04', () => {
    const weather = new Weather(.04);
    expect(weather.text).toBe('Schwere Stürme haben große Teile der Ernte vernichtet')
  })

  test('is great when random value equals >=.85', () => {
    const weather = new Weather(.85);
    expect(weather.text).toBe('Tolles Wetter, Rekordernte!')
  })

});